# rsvp-visualization-software #

## Modules are as follows: ##

    **First**: Drowsiness Detector
    **Second**: Visualize Signal
    **Third**: Bad Channel Detector [Channel Analyzer]
    **Forth**: Frequency Plot Visualize 
    **Fifth**: Event - Related Potential Plot [erpBCI]
    **Sixth**: Quick Analytics 

*** Soon this will contain instructions and suggestions for troubleshooting software *** 


## To open interface type *case sensitive*: ## 
    >> TestSignal

## To run Drowsiness Detector: ##
   >> drowsinessDetector
     




