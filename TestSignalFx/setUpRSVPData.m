function [afterFrontendFilterData, trialSampleTimeIndices, participantName, participantTrial, EEG, nuEpochs, freq, bgEEG] = setUpRSVPData(rawData,triggerSignal, sessionFolder, fs, segLength)
    
    % Based on the RSVP folder naming structure, take out the participant name
    C = strsplit(sessionFolder, '\');
    nameToString = C(1,end - 1);
    nameToString = strsplit(nameToString{1}, '_');
    participantName = nameToString(3);
    participantName = participantName{1};

    participantTrial = nameToString(4);
    participantTrial = participantTrial{1};

    load 'inputFilterCoef.mat';
    [afterFrontendFilterData, afterFrontendFilterTrigger]=applyFrontendFilter(rawData,triggerSignal,frontendFilter);
    
    % Run RSVPKeyboardParams
    run([sessionFolder 'RSVPKeyboardParameters.m']);
    % Load imageList
    vars = whos('-file',[sessionFolder 'taskHistory.mat']);
    % system(['7z x ' sessionFolder 'Parameters.zip imageList.xls' sessionFolder]);
    %The '7z x ' command is to unzip. This command could be different depending on the unzip software in the computer.
    load([sessionFolder 'taskHistory.mat']);
    imageStructs=xls2Structs([sessionFolder 'imageList.xls']);

    %Initialize the triggerPartitioner struct.
    triggerPartitioner.TARGET_TRIGGER_OFFSET = RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;
    triggerPartitioner.sequenceEndID = imageStructs(strcmpi({imageStructs.Name},'ERPSequenceEnd')).ID;
    triggerPartitioner.pauseID = imageStructs(strcmpi({imageStructs.Name},'Pause')).ID; %correct PauseID added to this folder already
    triggerPartitioner.fixationID = imageStructs(strcmpi({imageStructs.Name},'Fixation')).ID;
    triggerPartitioner.windowLengthinSamples = round(RSVPKeyboardParams.windowDuration.ERP*fs);
    triggerPartitioner.firstUnprocessedTimeIndex = 1;


    % Decode trigger
    [~,~,trialSampleTimeIndices,~,~]=...
        triggerDecoder(afterFrontendFilterTrigger,triggerPartitioner);

    % Define beginning (bg) and end of loaded EEG file
    bgEEG = trialSampleTimeIndices(1); % start when trail begins
    [col, ~] = size(afterFrontendFilterData);
    endEEG = col; 
    length = (endEEG - bgEEG); 
    nuEpochs = floor(length/segLength); % number of epochs to be scored 
    lengthDivisable = (nuEpochs*segLength); % lengh necessary to have perfectly reshapable matrix 
    start = abs(col-lengthDivisable+1);
    EEG = afterFrontendFilterData(start:endEEG, :);
    
    
    % calculate freq points for later calculation     
    freq=((0+fs/segLength):fs/segLength:fs); 


end