function [clean, cleanEnd] = rejectSequencesByFreq(nuOfActualEpochs, power, iPower, freq, participantName, participantTrial)

try
    totalPower = cell(1, nuOfActualEpochs);
    sixtyCyBand = cell(1, nuOfActualEpochs);
    gammaLowBand = cell(1, nuOfActualEpochs);
    gammaHighBand = cell(1, nuOfActualEpochs);
    noiseBand = cell(1, nuOfActualEpochs);

    for i = 1:nuOfActualEpochs
         if isempty (power{iPower}) == 0
            totalPower{i} = bandpower(power{i},freq, [freq(1) 50], 'psd');  
            sixtyCyBand{i} = bandpower(power{i},freq, [51 69.75], 'psd'); 
            gammaLowBand{i} = bandpower(power{i}, freq, [30 50.75], 'psd');
            gammaHighBand{i} = bandpower(power{i},freq, [70 90.75], 'psd'); 
            noiseBand{i} = bandpower(power{i},freq, [91 128], 'psd');
        else
            break
        end
    end

    %% Find Noisy and Clean Epochs

    % Creating empty arrays to append with noisy and clean intervals 
    noise = [];
    clean = [];

    for noisei = 1:nuOfActualEpochs

        if gammaLowBand{noisei} > (5E-06)
           noise = [noise noisei];
        elseif gammaHighBand{noisei} > (5E-06)
           noise = [noise noisei];
        elseif noiseBand{noisei} > (5E-06)
           noise = [noise noisei];
        elseif sixtyCyBand{noisei} > (5E-06)
           noise = [noise noisei];
        else
           clean = [clean noisei];
        end
    end
    
    % Find how many intervals we have left to deal with and choosing the clean 
    % epochs the column order of channels are as follows: Fz, Cz, Pz and Oz
    [startclean, endclean] = size(clean);


    % Display percent of clean data
    cleanEnd = endclean; cleanPercentDisp = ((cleanEnd/i)*100);
    
    % If enough data is clean, continue script and display message
    if cleanPercentDisp < 70
        disp('*** Noise Too High Do Not Include***')
        return
    elseif cleanPercentDisp > 70
        fprintf('*** Drowsiness Calculation for %s ', participantName);
        fprintf('trial#: %s *** \n', participantTrial);

    end

catch exception
    
    disp(exception);
    
    keyboard;
end