
% Loads file info from trial, **** requires the loadSessionData.m file in directory *****
[rawData,triggerSignal,fs,channelNames,filterInfo,daqInfos,sessionFolder]=loadSessionDataBin();
addpath(genpath(sessionFolder));


%% Define EEG parameters 

% Run RSVPKeyboardParams
run([sessionFolder 'RSVPKeyboardParameters.m']);

% % Run PresentationParams
% % run([sessionFolder 'presentationParameters.m']);
% % 
% % 
% % Run SignalProcessingParams
% % run([sessionFolder 'signalProcessingParameters.m']);

% Based on the RSVP folder naming structure, take out the participant name
C = strsplit(sessionFolder, '\');
nameToString = C(1,end - 1);
nameToString = strsplit(nameToString{1}, '_');
participantName = nameToString(3);
participantName = participantName{1};

participantTrial = nameToString(4);
participantTrial = participantTrial{1};


load 'inputFilterCoef.mat';
[afterFrontendFilterData, afterFrontendFilterTrigger]=applyFrontendFilter(rawData,triggerSignal,frontendFilter);

% Load imageList
vars = whos('-file',[sessionFolder 'taskHistory.mat']);
system(['unzip ' sessionFolder 'Parameters.zip imageList.xls -d ' sessionFolder]);
load([sessionFolder 'taskHistory.mat']);
imageStructs=xls2Structs([sessionFolder 'imageList.xls']);

%Initialize the triggerPartitioner struct.
triggerPartitioner.TARGET_TRIGGER_OFFSET = RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;
triggerPartitioner.sequenceEndID = imageStructs(strcmpi({imageStructs.Name},'ERPSequenceEnd')).ID;
triggerPartitioner.pauseID = imageStructs(strcmpi({imageStructs.Name},'Pause')).ID; %correct PauseID added to this folder already
triggerPartitioner.fixationID = imageStructs(strcmpi({imageStructs.Name},'Fixation')).ID;
triggerPartitioner.windowLengthinSamples = round(RSVPKeyboardParams.windowDuration.ERP*fs);
triggerPartitioner.firstUnprocessedTimeIndex = 1;


% Decode trigger
[~,completedSequenceCount,trialSampleTimeIndices,trialTargetness,trialLabels]=triggerDecoder(triggerSignal,triggerPartitioner);



%% 

% Define beginning (bg) and end of loaded EEG file
bgEEG = trialSampleTimeIndices(1); % start when trail begins
[col, row] = size(afterFrontendFilterData);
endEEG = col; 

% Split continuous EEG data into epochs
segLength = 4000; % desired segmentation length
length = (endEEG - bgEEG); nuEpochs = floor(length/segLength); % number of epochs to be scored 
lengthDivisable = (nuEpochs*segLength); % lengh necessary to have perfectly reshapable matrix 
start = abs(col-lengthDivisable+1);
EEG = afterFrontendFilterData(start:endEEG, :); % new EEG matrix to allow for segmentable matrix
channelNumber = 4; % defines the number of channel

% Used by other functions
cz = EEG(:,8);
p1 = EEG(:,1); %actually p3
p2 = EEG(:,7); %actually p4
disp(' . ');

%% Eye Blink Calculations

% Eye blink calculation
Fp1 = EEG(:,10);
Fp2 = EEG(:,11); 

% Average to avoid false positives due to noise
averageFps = (Fp1 +Fp2)/2;

% Inversing makes it easier to find peaks associated with blinks 
averageFps = (-averageFps);

% Find the eye blink. Min peak distance and height defined here -- 
[blinksAverage, locationAverage] = findpeaks(averageFps, 'MinPeakDistance', 100, 'MinPeakHeight', .00004);
[sizeLocation, vv] = size(locationAverage); 

% Calculate initial blink rate using countRateCalculation # of blinks, and
% overall blink rate using the length of time elapsed and the number of blinks
countRateCalculation = 10;
for blink = 1:(sizeLocation - 10)
    if locationAverage(blink) > bgEEG
        startLocation = blink;
        break
    end
end

if exist('startLocation', 'var')
    active = 1;
else
    startLocation = 25;
end 

blinkRateInitial = countRateCalculation / (locationAverage(startLocation + 10) - locationAverage(startLocation)); 
blinkReducationIntervals = [];

for bli = 1:(sizeLocation-10)
    newBlinkRate = countRateCalculation / (locationAverage(bli + 10) - locationAverage(bli));
    bli = bli + 10;
    if newBlinkRate < blinkRateInitial
        blinkReducationIntervals = [blinkReducationIntervals bli];
    end
end

% To avoid just random error in blink detection, levels of drowsiness are used based on the percent of time blink reduction is seen in the file
[rows, blinkLength] = size(blinkReducationIntervals);

if blinkLength/bli >= .30 && blinkLength/bli <= .4499999999
    eyeBlinkDrowsinessScore = 1; 
elseif blinkLength/bli >= .45 && blinkLength/bli <= .5999999999
    eyeBlinkDrowsinessScore = 2; 
elseif blinkLength/bli >= .6 && blinkLength/bli <= .749999999
    eyeBlinkDrowsinessScore = 3; 
elseif blinkLength/bli >= .75 && blinkLength/bli <= 1
    eyeBlinkDrowsinessScore = 4;
else     
    eyeBlinkDrowsinessScore = 0; 
end 

disp(' .. ');

%% Windowing and Epoching 

pzEST = (p1 +p2)/2; 
EEG = EEG(:,[4, 1, 7, 8]); %NOTE: if changing this, must change channel number Fz, Cz, P1, P2 

% calculate a hanning window
hanning = hann(segLength, 'symmetric');
hanning = repmat(hanning,1, channelNumber);

% EEG = EEG.*hanning; This is for applying hanning to whole data set


reformatEEG = cell2mat(num2cell(EEG)); ePOCHS = mat2cell(reformatEEG, repmat(segLength,nuEpochs,1), channelNumber); % creates a new matrix with nuEpochs containing segments of determined SegLength, referred to as ePOCHS
% clear endEEG start lengthDivisable col  % deletes unnecessary files in workspace 

reformatEEG = EEG;
%% For plotting

N=segLength; freq=(1:N)*fs/N; 


%% Runs FFT in sequence on the created ePOCHS of length segLength
 
nuEpochs = floor(length/segLength);
for i = 1:nuEpochs
    windowedEpochs{i} = ePOCHS{i}.*hanning;
    dataFFT{i} = fft(windowedEpochs{i}, segLength); 
    dataFFT{i} = mean(dataFFT{i},2);
    power{i} = (abs(dataFFT{i})).^2;
end 

disp(' ... ');

%% Takes bands of defined frequencies for Noise Calculation

for i = 1:nuEpochs
        totalPower{i} = bandpower(power{i},freq, [.1 50], 'psd');  
        sixtyCyBand{i} = bandpower(power{i},freq, [51 69.75], 'psd'); 
        gammaLowBand{i} = bandpower(power{i}, freq, [30 50.75], 'psd');
        gammaHighBand{i} = bandpower(power{i},freq, [70 90.75], 'psd'); 
        noiseBand{i} = bandpower(power{i},freq, [91 128], 'psd');
end

%% Find Noisy and Clean Epochs

% Creating empty arrays to append with noisy and clean intervals 
noise = [];
clean = [];

for noisei = 1:nuEpochs
    if gammaLowBand{noisei} > (5E-06)
       noise = [noise noisei];
    elseif gammaHighBand{noisei} > (5E-06)
       noise = [noise noisei];
    elseif noiseBand{noisei} > (5E-06)
        noise = [noise noisei];
    elseif sixtyCyBand{noisei} > (5E-06)
        noise = [noise noisei];
    else
       clean = [clean noisei];
    end
end

% Find how many intervals we have left to deal with 
cleanArray = power(clean);
badArray = power(~clean);
[startclean, endclean] = size(clean);

% Display percent of clean data
cleanEnd = endclean; cleanPercentDisp = ((cleanEnd/i)*100);
% fprintf('Clean Data Percent: %s \n', num2str(cleanPercentDisp));

% If enough data is clean, continue script and display message
if cleanPercentDisp < 70
    disp('*** Noise Too High ***')
    return
elseif cleanPercentDisp > 70
    fprintf('*** Drowsiness Calculation for %s ', participantName);
    fprintf('trial#: %s ***', participantTrial);
    
end

%% Loop through the clean intervals and determine alertness/ drowsiness

for r = 1:endclean
        totalPower{r} = bandpower(cleanArray{r},freq, [.1 50], 'psd');
        deltaBand{r} = bandpower(cleanArray{r},freq, [.1 3.75], 'psd');
        thetaBand{r} = bandpower(cleanArray{r},freq, [4 7.75], 'psd'); 
        alphaBand{r} = bandpower(cleanArray{r},freq, [8 12.75], 'psd');
        beta1Band{r} = bandpower(cleanArray{r},freq, [13 17.75], 'psd'); 
        beta2Band{r} = bandpower(cleanArray{r},freq, [18 29.75], 'psd');   
end

startingTheta = thetaBand{1} + (thetaBand{1} * .15); % looks for a 15% increase in theta
startingAlpha = alphaBand{1} - (alphaBand{1} * .15); % looks for a 15% decrease in alpha


significantChangeinState1 = [];
significantChangeinState2 = [];

for drowsyTest = 1:endclean
    if startingTheta < thetaBand{drowsyTest}
        significantChangeinState1 = [significantChangeinState1 drowsyTest];
    elseif startingAlpha > alphaBand{drowsyTest}
        significantChangeinState2 = [significantChangeinState2 drowsyTest];
    end 
end

[rows, stateChange] = size(significantChangeinState1);
[rows, stateChange2] = size(significantChangeinState2);

stateChange = stateChange + stateChange2; 

if stateChange/cleanEnd >= .30 && stateChange/cleanEnd <= .4499999999
    freqDrowsinessScore = 1; 
elseif stateChange/cleanEnd >= .45 && stateChange/cleanEnd <= .5999999999
    freqDrowsinessScore = 2; 
elseif stateChange/cleanEnd >= .60 && stateChange/cleanEnd <= .7499999999
    freqDrowsinessScore = 3; 
elseif stateChange/cleanEnd >= .75 && stateChange/cleanEnd <= 1
    freqDrowsinessScore = 4;
else     
    freqDrowsinessScore = 0; 
end 


% [[% TO-DO %]] Look for the anteriorization of alpha (Occiptal) ---> (Central and
% Frontal) and at median power frequency 


%% Calculate Drowsiness Score

% average score for normal user
drowsinessScore = num2str((eyeBlinkDrowsinessScore + freqDrowsinessScore)/2); 
fprintf('\n');
fprintf('\n');
fprintf('Drowsiness Score: %s \n', drowsinessScore);
fprintf('Frequency Score: %d \n', freqDrowsinessScore);
fprintf('Eye Blink Rate Score: %x \n', eyeBlinkDrowsinessScore);


% [[% TO-DO %]] weight scores differently depending on different user
% archetecture at start
