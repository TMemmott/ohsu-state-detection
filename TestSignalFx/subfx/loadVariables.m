%% Load Data

% Loads file info from trial, **** requires the loadSessionData.m file in directory *****
[rawData,triggerSignal,fs,channelNames,filterInfo,daqInfos,sessionFolder]=loadSessionDataBin();
addpath(genpath(sessionFolder));
