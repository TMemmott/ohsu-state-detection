function [totalPower, deltaBand, thetaBand, alphaBand, anteriorAlphaBand, beta1Band, beta2Band] = calculatePowerBands(cleanArray, AntCleanArray, endclean, freq)
%% Loop through the clean intervals and determine power spectral density   
%     for the bands of interest and for 
try
    deltaBand  = cell(1, endclean);
    thetaBand  = cell(1, endclean); 
    alphaBand  = cell(1, endclean);
    anteriorAlphaBand  = cell(1, endclean);
    beta1Band  = cell(1, endclean);
    beta2Band  = cell(1, endclean);
    totalPower = cell(1, endclean);

    for r = 1:endclean
        totalPower{r} = bandpower(cleanArray{r}, freq, [freq(1) 50], 'psd');
        
        deltaBand{r} = bandpower(cleanArray{r}, freq, [freq(1) 3.75], 'psd');
        thetaBand{r} = bandpower(cleanArray{r}, freq, [4 7.75], 'psd'); 
        alphaBand{r} = bandpower(cleanArray{r}, freq, [8 12.75], 'psd');
        % calculating the alpha band power in each channel
        anteriorAlphaBand{r} = bandpower(AntCleanArray{r}, freq, [8 12.75], 'psd');
        beta1Band{r} = bandpower(cleanArray{r}, freq, [13 17.75], 'psd'); 
        beta2Band{r} = bandpower(cleanArray{r}, freq, [18 29.75], 'psd');   
    end

catch exception
    
    disp(exception);
    
    keyboard;

end