function  [freqDrowsinessScoreAlpha, freqDrowsinessScoreTheta] = getFreqDrowsinessScores(cleanEnd, startingTheta, thetaBand, startingAlpha, alphaBand)
%getFreqDrowsinessScores gets frequency based drowsiness measures and
% scores them
significantChangeinState1 = [];
significantChangeinState2 = [];

    for drowsyTest = 1:cleanEnd
            
            if startingTheta < thetaBand{drowsyTest}
                significantChangeinState1 = [significantChangeinState1 drowsyTest];
            end 
            
            if startingAlpha > alphaBand{drowsyTest}
                significantChangeinState2 = [significantChangeinState2 drowsyTest];
            end
            
    end
    
                % See how many epochs resulted in a change in state flag          
            [~, stateChange] = size(significantChangeinState1);
            [~, stateChange2] = size(significantChangeinState2);
            
            % calculate the percentage change         
            percentChange = stateChange/cleanEnd;
            percentChange2 = stateChange2/cleanEnd;
            
            % loop through the theta band to score
            if percentChange >= .30 && percentChange <= .4499999999
                freqDrowsinessScoreTheta = 1; 
            elseif percentChange >= .45 && percentChange <= .5999999999
                freqDrowsinessScoreTheta = 2; 
            elseif percentChange >= .60 && percentChange <= .7499999999
                freqDrowsinessScoreTheta = 3; 
            elseif percentChange >= .75 && percentChange <= 1
                freqDrowsinessScoreTheta = 4;
            else     
                freqDrowsinessScoreTheta = 0; 
            end 
            
            % loop through the alpha band to score
            if percentChange2 >= .30 && percentChange2 <= .4499999999
                freqDrowsinessScoreAlpha = 1; 
            elseif percentChange2 >= .45 && percentChange2 <= .5999999999
                freqDrowsinessScoreAlpha = 2; 
            elseif percentChange2 >= .60 && percentChange2 <= .7499999999
                freqDrowsinessScoreAlpha = 3; 
            elseif percentChange2 >= .75 && percentChange2 <= 1
                freqDrowsinessScoreAlpha = 4;
            else     
                freqDrowsinessScoreAlpha = 0; 
            end 

end

