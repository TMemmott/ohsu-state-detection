function [segLength, adaptiveEpocherEnabled, alternateWeightingEnabled, medication, anteriorizationOfAlpha, MedianFrequencyOfPower, lengthOfEpoch, eightSec, thirtyTwoSec, thirteenHz, channelNumber, freq] = getDrowsinessParameters(fs)

    % Desired segmentation length for each epoch and booleans for calculations
    desTimeSec = 10;
    segLength = desTimeSec * 300;
    adaptiveEpocherEnabled = false;
    alternateWeightingEnabled = false;
    medication = false;
    anteriorizationOfAlpha = false;
    MedianFrequencyOfPower = true;
    
    % defines the number of channels to use in frequency calculations
    channelNumber = 2; 
  
    if segLength == 4000 %if we are epoching every 4000 msec, then
        lengthOfEpoch = 28;% two trial segments, each of 14 triggers long, make 28
        eightSec = 2; % 2 epochs equal 8 sec
        thirtyTwoSec = 4; %8 epochs equal 32 sec
        thirteenHz = 124;
        disp('Segment Lengths are set at 4 sec.');
    else 
        lengthOfEpoch = 14;%This is for the case we are epoching every 2000 msec
        eightSec = 4; % 4 epochs equal 8 sec
        thirtyTwoSec = 8; %8 epochs equal 32 sec
        thirteenHz = 124;
        disp('Segment Lengths are set at 2 sec.');
    end

end