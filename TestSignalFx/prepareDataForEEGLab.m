%  This is a script to prepare RSVP data for import into EEGLab
%   It will output a .mat file called ``trailData_**participant info**.mat`` 
%   and a .txt file called ``trialTrigger_**participant info**.txt``


% Loads file info from trial data and adds the folder location to path
[rawData,triggerSignal,fs,channelNames,filterInfo,daqInfos,sessionFolder]=loadSessionDataBin();
addpath(genpath(sessionFolder));


headers = {'Type', 'Latency', 'Duration'};

% get all the trigger data
[afterFrontendFilterData, completedSequenceCount, trialSampleTimeIndices, ...
    trialTargetness, trialLabels,  participantName, participantTrial] = setUpRSVPData(rawData,triggerSignal, sessionFolder, fs);

%% Save Raw Data

% save a matlab file with raw data values from eeg
participantInfo = strcat(participantName, '_', participantTrial);
trialDataName = strcat('trialData_', participantInfo);
fileDataName = strcat(sessionFolder, trialDataName, '.mat');
save(fileDataName, 'afterFrontendFilterData')

% % save a matlab file with filtered raw data values from eeg with triggers
% save('trailDatawithTrigger.mat', 'afterFrontendFilterData')

%% Save Trigger Data

% run presentation parameters and get the ERP trial length
run([sessionFolder 'presentationParameters.m']);
duration = num2str(presentationStruct.Duration.ERPTrial);

%  assign headers to cell to be saved as trigger
cell = headers;

[col, lengthSample] = size(trialSampleTimeIndices);
% loop throught the trial smaples and assign the next row of data for
% triggers
for i = 1:(lengthSample)
    latency = num2str(trialSampleTimeIndices(i));
    if trialTargetness(i) == 1
        data = {'target', latency, duration};
    else
        data = {'nontarget', latency, duration};
    end
    
    cell(i,:) = data;
    
end


trialTriggerName = strcat('trialTrigger_', participantInfo);
fileTriggerName = strcat(sessionFolder, trialTriggerName, '.txt');
% save(fileTriggerName, 'cell')

T = cell2table(cell, 'VariableNames', headers);
writetable(T, fileTriggerName)