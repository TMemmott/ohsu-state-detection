%% Noise Parameters: 

gammaThreshold = 5E-06;
higherFrequencyThreshold = 5E-07; 
totalPowerThershold = 5.5E-05; 
dataSegmentationLength = 1500;
channelNumber = 3;
fs = 300;


%% Load Data

% Retrieving file, may be handled in the GUI later, but useful to test here
[rawData,triggerSignal,sampleRate,channelNames,filterInfo,daqInfos]=loadSessionDataBin();
disp('Data is loaded');
load 'inputFilterCoef.mat';
[afterFrontendFilterData, afterFrontendFilterTrigger]=applyFrontendFilter(rawData,triggerSignal,frontendFilter);

freq=(1:dataSegmentationLength)*fs/dataSegmentationLength;

%% Split continuous EEG data into epochs
%   This script does not take into account the start of the RSVP task. 
%   To start segemenation with respect to beginning of task, the correct RSVP trigger decoder will be needed.

bgEEG = 1000; % consider latency of 10-15 seconds or make this changeable
[col, row] = size(afterFrontendFilterData);
endEEG = col; 

length = (endEEG - bgEEG); nuEpochs = floor(length/dataSegmentationLength); % number of epochs to be inspected 
lengthDivisable = (nuEpochs*dataSegmentationLength); % length necessary to have perfectly reshapable matrix 
start = abs(col-lengthDivisable+1);
EEG = afterFrontendFilterData(start:endEEG, :); % new EEG matrix to allow for segmentable matrix

%% Windowing and Epoching 

% calculate a hanning window
hanning = hann(dataSegmentationLength, 'symmetric');
hanning = repmat(hanning,1, channelNumber);

% EEG = EEG.*hanning; This is for applying hanning window to whole data set. An alternative to a segemenation approach

reformatEEG = cell2mat(num2cell(EEG)); ePOCHS = mat2cell(reformatEEG, repmat(dataSegmentationLength,nuEpochs,1), channelNumber); % creates a new matrix with nuEpochs containing segments of determined SegLength, referred to as ePOCHS
clear bgEEG endEEG start lengthDivisable col  

%% Runs FFT in sequence on the created ePOCHS of length segLength
 
for i = 1:nuEpochs
    windowedEpochs{i} = ePOCHS{i}.*hanning; % apply hanning window
    dataFFT{i} = fft(windowedEpochs{i}, dataSegmentationLength); % FFT
    dataFFT{i} = mean(dataFFT{i},2);
    power{i} = (abs(dataFFT{i})).^2; % Power
end 

%% Takes power bands of desired frequencies for Noise Calculation

disp('Calculating noise...');
for i = 1:nuEpochs
        totalPower{i} = bandpower(power{i},freq, [1 128], 'psd');  
        noiseBand{i} = bandpower(power{i},freq, [30 128], 'psd'); 
        gammaBand{i} = bandpower(power{i}, freq, [30 50.75], 'psd');
end

%% Find Noisy and Clean Epochs

% Creating empty arrays to append with noisy and clean intervals 
noise = [];
clean = [];

for noiseI = 1:nuEpochs
    if gammaBand{noiseI} > gammaThreshold
       noise = [noise noiseI];
    elseif totalPower{noiseI} > totalPowerThershold
        noise = [noise noiseI];
    elseif noiseBand{noiseI} > higherFrequencyThreshold
        noise = [noise noiseI];
    else
       clean = [clean noiseI];
    end
end


% Seperate good and bad arrays
% cleanArray = power(clean);
% badArray = power(~clean);

% Find how many intervals we have left to deal with 
[startclean, endclean] = size(clean);

% Display percent of clean data
cleanEnd = endclean; cleanPercentDisp = num2str(((cleanEnd/i)*100));
disp([cleanPercentDisp,'% of your data is clean']);