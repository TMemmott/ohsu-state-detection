function createfigure(zdata1)
%CREATEFIGURE(ZDATA1)
%  ZDATA1:  surface zdata

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1,...
    'XTickLabel',{'C3','F3','Fz','F4','C4','P4','Cz','A1','Fp1','Fp2','T3','T5','O1','O2','F7','F8','A2','T6','T4'},...
    'XTick',[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20]);
    
view(axes1,[1.5 6]);
grid(axes1,'on');
hold(axes1,'all');

% Create mesh
mesh(zdata1,'Parent',axes1);

