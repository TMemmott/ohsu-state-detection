%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% erpBCI: This is an ERP ploting module for RSVP calibration data
%   simply call >> erpBCI in the command line
%       If a calibration file with a taskhistory.mat is present
%       erpBCI will produce the resulting target/non-target display. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Loading data...');
        
% Load session data
[rawData,triggerSignal,fs,channelNames,filterInfo,daqInfos,sessionFolder]=loadSessionDataBin();

addpath(genpath(sessionFolder));
% % Unzip all of Parameters file
paramsFileDir = sessionFolder;
unzip('Parameters.zip', paramsFileDir); 

% Run RSVPKeyboardParams
run([paramsFileDir 'RSVPKeyboardParameters.m']);

% Run PresentationParams
run([paramsFileDir 'presentationParameters.m']);

% Run SignalProcessingParams
run([paramsFileDir 'signalProcessingParameters.m']);

% Load imageList
vars = whos('-file',[sessionFolder 'taskHistory.mat']);
system(['unzip ' sessionFolder 'Parameters.zip imageList.xls -d ' sessionFolder]);
load([sessionFolder 'taskHistory.mat']);
imageStructs=xls2Structs([sessionFolder 'imageList.xls']);

%Initialize the triggerPartitioner struct.
triggerPartitioner.TARGET_TRIGGER_OFFSET = RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;
triggerPartitioner.sequenceEndID = imageStructs(strcmpi({imageStructs.Name},'ERPSequenceEnd')).ID;
triggerPartitioner.pauseID = imageStructs(strcmpi({imageStructs.Name},'Pause')).ID; %correct PauseID added to this folder already
triggerPartitioner.fixationID = imageStructs(strcmpi({imageStructs.Name},'Fixation')).ID;
triggerPartitioner.windowLengthinSamples = round(RSVPKeyboardParams.windowDuration.ERP*fs);
triggerPartitioner.firstUnprocessedTimeIndex = 1;


%% Filtering and retrival of trigger information 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the inputFilterCoef.mat file.
load 'inputFilterCoef.mat';

% Apply frontend filter
[afterFrontendFilterData, afterFrontendFilterTrigger]=applyFrontendFilter(rawData,triggerSignal,frontendFilter);

% % Optional low pass filter 
% fc = 15; % desired cut off frequency in Hz
% fn = fs/2; % Nyquivst frequency = sampling frequency/2;
% order = 2; % 2nd order filter, low
% [b14, a14] = butter(order, (fc/fn), 'low');
% afterFrontendFilterData=filtfilt(b14,a14,rawData); % filtfilt removes phase distortion
% 
% % Optional low pass filter 
% fc = 1; % desired cut off frequency in Hz
% fn = fs/2; % Nyquivst frequency = sampling frequency/2;
% order = 2; % 2nd order filter, low
% [b14, a14] = butter(order, (fc/fn), 'high');
% afterFrontendFilterData=filtfilt(b14,a14,afterFrontendFilterData); % filtfilt removes phase distortion

% afterFrontendFilterData=filtfilt(b14,a14,rawData); % removes phase distortion  

% Decode trigger
if exist('matrixSequences','var')
    [~,completedSequenceCount,trialSampleTimeIndices,trialTargetness,trialLabels]=triggerDecoder(afterFrontendFilterTrigger,triggerPartitioner,matrixSequences);
else
    [~,completedSequenceCount,trialSampleTimeIndices,trialTargetness,trialLabels]=triggerDecoder(afterFrontendFilterTrigger,triggerPartitioner);
end

%% Window data into trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% makes a file that determines the start and end of an epoch based on the window (wn) size 
lenT = length(trialSampleTimeIndices);

% Get central channels for ERP plot
cz = afterFrontendFilterData(:,8);
p1 = afterFrontendFilterData(:,1);
p2 = afterFrontendFilterData(:, 7);
pzEst = ((p1+p2)/2); 

% Use an average of the Pz and Cz channels for ERP plotting
afterFrontendFilterData = ((cz + pzEst)/2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop through data and create reformatted (windowed ERP) data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:(lenT)
    trialStart{i} = trialSampleTimeIndices(1, i); %finds the start of the epoch
    trialEnd{i} = trialStart{i} + 900; %finds the end of the epoch
    trialStartplusPreStim{i} = trialStart{i} - 100; %get 100ms prestim information
    index{i} = trialStartplusPreStim{i}:trialEnd{i}; %makes an index from the start and end
    reformatData{i} = afterFrontendFilterData(index{i},:); %reformats into epochs 
end

%% Target/ non-target Calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Makes sure the index for ERPS does not exceed reformatData
[ex, sizeofData] = size(reformatData);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Makes arrays of target and nontarget elements %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get Targets and # presented
targetSegments = find(trialTargetness); 
[ex, sizeofTargets] = size(targetSegments);

% Check the number of targets does not exceed data
if sizeofTargets > sizeofData
    extraTargets = sizeofTargets - sizeofData;
    targetSegments = targetSegments(1:end - extraTargets);
else
    targetSegments = targetSegments(1:end);
end 

% Get Non-Targets and # presented
nontargetSegments = find(~trialTargetness);
[ex, sizeofNonTargets] = size(nontargetSegments);

% Check the number of non-targets does not exceed data
if sizeofNonTargets > sizeofData
    extraNonTargets = sizeofNonTargets - sizeofData;
    nontargetSegments = nontargetSegments(1:end - extraNonTargets);
else
    nontargetSegments = nontargetSegments(1:end);
end 

targetArray = reformatData(targetSegments);
nontargetArray = reformatData(nontargetSegments);

%Average for Targets 
dimA = ndims(targetArray{1});         % gets the dimenstion of array  
Ftarget = cat(dimA+1,targetArray{:}); % convert to a dimenstional matrix 
targetAverage = - mean(Ftarget,dimA+1); % average

%Average for nonTargets
dimB = ndims(nontargetArray{1});            % gets the dimenstion of array  
Fnontarget = cat(dimB+1,nontargetArray{:}); % convert to a dimenstional matrix         
nontargetAverage = - mean(Fnontarget,dimB+1); % average

%% Peaks and Frequency Information

% 
% fc=40;% cut off frequency
% fn=fs/2; %nyquist frequency = sample frequency/2;
% order = 2; %2nd order filter
% [b14, a14]=butter(order,(fc/fn),'low'); %buttersworth, low pass
% 
% % Find P200
% targetAveragePeaks=filtfilt(b14,a14,targetAverage);
% [findTargetPeaks, findTargetLocation] = findpeaks(targetAveragePeaks, 'MinPeakHeight', .00000035, 'Npeaks', 1);
% P200= findTargetLocation;
% 
% % Find N1, N2
% targetAveragePeaks = -targetAveragePeaks;
% [findTargetPeaks2, findTargetLocation2] = findpeaks(targetAveragePeaks, 'MinPeakHeight', .0000004, 'Npeaks', 2); 
% N100 = findTargetLocation2(1);
% N200 = findTargetLocation2(2);

% exploring erp freq information

% [len, row] = size(nontargetAverage); %gets size of erps
% freq=(1:len)*fs/len; %gets information
% nontargetfreqData = (abs(fft(nontargetAverage).^2));
% targetfreqData = (abs(fft(targetAverage).^2));
% 
% presentationAverage = (nontargetAverage + targetAverage)/2;
% presentationAverageFreq = (abs(fft(presentationAverage))).^2;
% figure
% plot(freq, presentationAverageFreq);
% 
% trialSampleTimeStart = trialSampleTimeIndices(1)-1000;
% cz = cz((2000:trialSampleTimeStart),:);
% czFreq = (abs(fft(cz))).^2;
% 
% [czlen row] = size(czFreq);
% freqcZ =(1:czlen)*fs/czlen;
% figure
% plot(freqcZ, czFreq)

%% Figure 

% Create figure
figure1 = figure('Name','P300 Event Related Potential',...
    'Color',[0.313725501298904 0.313725501298904 0.313725501298904]);

% Create axes
axes1 = axes('Parent',figure1,...
    'ZColor',[0.831372559070587 0.815686285495758 0.7843137383461],...
    'YGrid','on',...
    'YColor',[0.831372559070587 0.815686285495758 0.7843137383461],...
    'XColor',[0.831372559070587 0.815686285495758 0.7843137383461],...
    'Position',[0.0802833530106257 0.140186915887851 0.853600944510035 0.784813084112151],...
    'FontWeight','light',...
    'FontSize',11,...
    'FontName','Times',...
    'FontAngle','italic',...
    'Color',[0.941176474094391 0.941176474094391 0.941176474094391]);

xlim(axes1,[0 1000]);
box(axes1,'on');
hold(axes1,'all');

% Create xlabel
xlabel({'Time (milliseconds)'},'FontWeight','light','FontSize',16,...
    'FontName','Times',...
    'FontAngle','italic',...
    'Color',[0.862745106220245 0.862745106220245 0.862745106220245],...
    'BackgroundColor',[0.313725501298904 0.313725501298904 0.313725501298904]);

% Create ylabel
ylabel('Voltage','FontWeight','light','FontSize',16,'FontName','Times',...
    'FontAngle','italic',...
    'Color',[0.862745106220245 0.862745106220245 0.862745106220245]);

% Pre-stimulus line
annotation(gcf,'line',[0.221603954081632 0.222385204081632],...
    [0.927677329624478 0.132127955493741],'LineStyle','-.');

% Plot Target
plot(targetAverage, 'LineWidth',1.5,'Color',[1 0 0],'DisplayName','Target')
hold on

% Plot Non- Target
plot(nontargetAverage, 'LineWidth',1,'Color',[0 0 1],'DisplayName','Non-Target')
set(gca,'YDir','reverse')

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'EdgeColor',[0.501960813999176 0.501960813999176 0.501960813999176],...
    'Position',[0.729575663944584 0.817633158810855 0.145218417945691 0.0785046728971963],...
    'Color',[0.905882358551025 0.905882358551025 0.905882358551025]);

%% For Peak Labels

% hn = text(findTargetLocation, findTargetPeaks,[' P2  '],...
%     'HorizontalAlignment','Right','Rotation',25);
% hm = text(findTargetLocation2(1), -findTargetPeaks2(1),[' N1  '],...
%     'HorizontalAlignment','Right','Rotation',25);
% hl = text(findTargetLocation2(2), -findTargetPeaks2(2),[' N2 '],...
%     'HorizontalAlignment','Right','Rotation',25);

hold off
