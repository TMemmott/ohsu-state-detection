function varargout = intervals(varargin)
% INTERVALS MATLAB code for intervals.fig
%      INTERVALS, by itself, creates a new INTERVALS or raises the existing
%      singleton*.
%
%      H = INTERVALS returns the handle to a new INTERVALS or the handle to
%      the existing singleton*.
%
%      INTERVALS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERVALS.M with the given input arguments.
%
%      INTERVALS('Property','Value',...) creates a new INTERVALS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before intervals_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to intervals_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @intervals_OpeningFcn, ...
                   'gui_OutputFcn',  @intervals_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before intervals is made visible.
function intervals_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to intervals (see VARARGIN)

% Choose default command line output for intervals
handles.output = hObject;

% UIWAIT makes intervals wait for user response (see UIRESUME)
% uiwait(handles.figure1);

assignin('base', 'k' , 1);
k = evalin('base','k');
freq = evalin('base','freq');
ePOCHS = evalin('base','power');
handles.current_data = cell2mat(ePOCHS(:,k));

handles.line = plot(freq(1:2000), handles.current_data(1:2000));



% Update handles structure
guidata(hObject, handles);

%make a switch between power and ePOCHS!!!!!!!! The ERP view will need more
%help from the NE team because it requires a complex code for trigger
%decoding

% also making a button that when pushed logged the k would allow for sleep
% scoring of the intervals, because I could find the correct one. making it
% then index the ePOCH info (copy into a predetermined folder would be
% exactly what I've been talking about. 


% --- Outputs from this function are returned to the command line.
function varargout = intervals_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nuEpochs = evalin('base','nuEpochs');
ePOCHS = evalin('base','power');
k = evalin('base','k');

if k >= 1 && k ~= nuEpochs
      k = k + 1;
      n = k;
else
    k = nuEpochs;
    n= k;
    disp('End of recording');
end
assignin('base', 'k', n);
k = evalin('base','k');
freq = evalin('base','freq');

handles.current_data = cell2mat(ePOCHS(:,k));

handles.line = plot(freq(1:2000), handles.current_data(1:2000));

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nuEpochs = evalin('base','nuEpochs');
ePOCHS = evalin('base','power');
k = evalin('base','k');

if k > 1 && k ~= 1
      k = k - 1;
      n = k;
else
    k = 1;
    n=k;
end

assignin('base', 'k', n);
k = evalin('base','k');
freq = evalin('base','freq');


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
v = evalin('base', 'reformatEEG');
channelInspect(v)
createfigure(v)

handles.current_data = cell2mat(ePOCHS(:,k));
handles.line = plot(freq(1:2000), handles.current_data(1:2000));


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
