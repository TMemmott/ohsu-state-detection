function channelInspect(zdata1)
% Create figure
figure1 = figure('Color',...
    [0.960784316062927 0.976470589637756 0.992156863212585]);
colormap('hsv');

% Create axes
axes1 = axes('Parent',figure1,...
    'XTickLabel',{'C3','F3','Fz','F4','C4','P4','Cz','A1','Fp1','Fp2','T3','T5','O1','O2','F7','F8','A2','T6','T4'},...
    'XTick',[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20],...
    'Position',[0.00494791666666668 0.115709500502068 0.985416666666667 0.857302208265323],...
    'PlotBoxAspectRatio',[1 1 1],...
    'MinorGridLineStyle','-',...
    'Color',[0.8 0.8 0.8],...
    'CameraViewAngle',10.339584907202,...
    'AmbientLightColor',[0.972549019607843 0.972549019607843 0.972549019607843]);

%% Axes

xlim(axes1,[0 17]);
view(axes1,[-30.5 28]);
zlim(axes1, [-.3 .3]);
grid(axes1,'on');
hold(axes1,'all');

% Create mesh
mesh(zdata1,'Parent',axes1);

% Create xlabel
xlabel('channels');

% Create zlabel
zlabel('Voltage');

% Create ylabel
ylabel('trial progression (ms)');

% Create colorbar
colorbar('peer',axes1);
