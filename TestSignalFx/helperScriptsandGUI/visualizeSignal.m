%%
function varargout = visualizeSignal(varargin)
% VISUALIZESIGNAL MATLAB code for visualizeSignal.fig
%      VISUALIZESIGNAL, by itself, creates a new VISUALIZESIGNAL or raises the existing
%      singleton*.
%
%      H = VISUALIZESIGNAL returns the handle to a new VISUALIZESIGNAL or the handle to
%      the existing singleton*.
%
%      VISUALIZESIGNAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VISUALIZESIGNAL.M with the given input arguments.
%
%      VISUALIZESIGNAL('Property','Value',...) creates a new VISUALIZESIGNAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before visualizeSignal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to visualizeSignal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help visualizeSignal

% Last Modified by GUIDE v2.5 20-May-2013 12:34:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @visualizeSignal_OpeningFcn, ...
                   'gui_OutputFcn',  @visualizeSignal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% --- Executes just before visualizeSignal is made visible.
function visualizeSignal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to visualizeSignal (see VARARGIN)

% Choose default command line output for visualizeSignal
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

scale_list = {'5','10','50','100','500','1000','10000'};
set(handles.scale_list,'String',scale_list);
set(handles.scale_list,'Value',4);
fileName = 'filterList.txt';

if exist(fileName,'file')
    fid = fopen(fileName,'r');
    tline = fgetl(fid);
    while ischar(tline)
        if exist(tline)
            loadFilter(tline,handles);
        end
        tline = fgets(fid);
    end
    set(handles.Filter,'Value',1);
end






% UIWAIT makes visualizeSignal wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% --- Outputs from this function are returned to the command line.
function varargout = visualizeSignal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%%
function start_index_Callback(hObject, eventdata, handles)
% hObject    handle to start_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of start_index as text
%        str2double(get(hObject,'String')) returns contents of start_index as a double
global mat;
global sampleRate;

[r,c] = size(mat);

if r == 0
    return
end

start_index = get(handles.start_index,'String');
if isempty(start_index)
    start_index = '0';
end

start_index = str2num(start_index);
start_index = start_index;

interval_length = get(handles.interval_length,'String');

interval_length = str2num(interval_length);
interval_length = interval_length;

if start_index < 0 || start_index >= floor(c/sampleRate);
    
   start_index = 0; 
   
   start_inv = num2str(start_index);
   
   set(handles.start_index,'String',start_inv);
   
end

end_index = start_index + interval_length;

if end_index*sampleRate > c
    
    end_index = floor(c/sampleRate); 
    
end

if interval_length > floor(c/sampleRate) - start_index
   
    interval_length = floor(c/sampleRate) - start_index;
    
    inv_len = num2str(interval_length);
    
    set(handles.interval_length,'String',inv_len);
    
end


graphUpdate(handles);


%% --- Executes during object creation, after setting all properties.
function start_index_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function interval_length_Callback(hObject, eventdata, handles)
% hObject    handle to interval_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of interval_length as text
%        str2double(get(hObject,'String')) returns contents of interval_length as a double
global mat;
global sampleRate;

[r c] = size(mat);

if r == 0 
   return ; 
end

start_index = get(handles.start_index,'String');

start_index = str2num(start_index);
start_index = start_index;

interval_length = get(handles.interval_length,'String');
%non-empty
if isempty(interval_length)
    interval_length = '1';
    set(handles.interval_length,'String',interval_length);
end

interval_length = str2num(interval_length);
interval_length = interval_length;
%greater than zero
if interval_length <= 0
    interval_length = '1';
    set(handles.interval_length,'String',interval_length);
end
%within the range
if interval_length > c/sampleRate - start_index
   
    interval_length = c/sampleRate - start_index;
    
    inv_len = num2str(interval_length);
    
    set(handles.interval_length,'String',inv_len);
end

graphUpdate(handles);



%% --- Executes during object creation, after setting all properties.
function interval_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to interval_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% --- Executes on selection change in scale_list.
function scale_list_Callback(hObject, eventdata, handles)
% hObject    handle to scale_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns scale_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from scale_list

graphUpdate(handles); 




%% --- Executes during object creation, after setting all properties.
function scale_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scale_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% --- Executes on button press in next_interval.
function next_interval_Callback(hObject, eventdata, handles)
% hObject    handle to next_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mat;
global sampleRate;

[r,c] = size(mat);

if r == 0
   return; 
end

start_index = get(handles.start_index,'String');

start_index = str2num(start_index);

interval_length = get(handles.interval_length,'String');

interval_length = str2num(interval_length);

start_index = start_index + interval_length;

if start_index > c/sampleRate - interval_length
   if c/sampleRate - interval_length > 0
      start_index = c/sampleRate - interval_length; 
   else
      start_index = 0;
   end
   
end

start_inv = num2str(start_index);

set(handles.start_index,'String',start_inv);
graphUpdate(handles);



%% --- Executes on button press in previous_interval.
function previous_interval_Callback(hObject, eventdata, handles)
% hObject    handle to previous_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mat;
global sampleRate;

[r, c] = size(mat);

if r == 0
   return; 
end

start_index = get(handles.start_index,'String');

start_index = str2num(start_index);

interval_length = get(handles.interval_length,'String');

interval_length = str2num(interval_length);

start_index = start_index - interval_length;

if start_index <= 0
    
   start_index = 1; 
   
end

start_inv = num2str(start_index);

set(handles.start_index,'String',start_inv);
graphUpdate(handles);


%%
function str = getCurrentPopupString(hh)
%# getCurrentPopupString returns the currently selected string in the popupmenu with handle hh

%# could test input here
if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
error('getCurrentPopupString needs a handle to a popupmenu as input')
end

%# get the string - do it the readable way
list = get(hh,'String');
val = get(hh,'Value');
str = list{val};
%%
function graphUpdate(handles)
global mat;
global trigMat;
global subPlotCount;
global sampleRate;
global channelNames;
global filterCoef;
global filterGrpDelay;


%get matrix size
[r c] = size(mat);
[r1 c1] = size(trigMat);

if r == 0
    return;
end

%scale
scale_sel = getCurrentPopupString(handles.scale_list);
scale_sel = str2num(scale_sel);
%filter 
filterOption = get(handles.Filter,'Value');
filterSize = get(handles.Filter,'String');
if length(filterSize) > 2
    temp = filterCoef(1,filterOption,:);
    h = temp(:)';
end

%determine number of plots
if r1 > 0
    subPlotCount = r + 1;
else
    subPlotCount = r;
end

%convolution to determine the output signal
if filterOption > 2
   for i = 1: r
      output(i,:) = conv(mat(i,:),h);
   end
else
    output = mat;
end

%find the trigger signal reduction factor
[coefficient,exponent] = strread(strrep(sprintf('%E',mean(output(1,:))),'E','#'),'%f#%f');
[coe, exp] = strread(strrep(sprintf('%E',mean(trigMat(:))),'E','#'),'%f#%f');

if any(trigMat > 128)
    if filterOption > 2
        trigFactor = 10^(exponent/(exp+1)*2+3.5);
    else 
        trigFactor = 10^(exponent/(exp+1)*2+0.5);
    end
    trigFactor1 = trigFactor*scale_sel;
else
    if filterOption > 2
        trigFactor = 10^(exponent/(exp+1)+5);
    else 
        trigFactor = 10^(exponent/(exp+1)+2);
    end
    trigFactor1 = trigFactor*scale_sel;
end

%determine each graph mean and std
for i = 1:subPlotCount
    if subPlotCount ~= r && i == subPlotCount
        stdList(i) = std(trigFactor1.*trigMat(1,:));
        meanList(i) = mean(trigFactor1.*trigMat(1,:));
    else
        stdList(i) = std(output(i,:));
        meanList(i) = mean(output(i,:));
    end
end
%ylim.ymin
ymin = 0;
%ylim.ymax
ymax = scale_sel*subPlotCount*10^(-6);

%ytick
level = (ymax -ymin)/subPlotCount;
ytick = ymin:level:ymax-level;

%get start_index
start_index = get(handles.start_index,'String');
start_index = str2num(start_index);
start_index = start_index;
start_index = start_index*sampleRate;

%get interval_length
interval_length = get(handles.interval_length,'String');
interval_length = str2num(interval_length);
interval_length = interval_length;

%get end_index
end_index = start_index + interval_length*sampleRate;

%apply group delay
if filterOption > 2
   number_of_delay = filterGrpDelay(1,filterOption,1);
else
    number_of_delay = 0;
end

if number_of_delay > 0
    trigOutput = [zeros(1,number_of_delay) trigMat];
else 
    trigOutput = trigMat;
end

%plot
signalLabel = cell([1 subPlotCount]);
for i=1:subPlotCount
   if subPlotCount ~= r && i == 1
       h = plot(ytick(i)+trigFactor1.*trigOutput);
       str = 'Trg';
       signalLabel(i) = cellstr(str);
       set(h,'linewidth',2);
   else
       plot(ytick(i)+output(subPlotCount-i+1,:));
       signalLabel(i) = channelNames(1,subPlotCount-i+1); 
   
   end
   hold all 
end

%format graph
%set ytick and yticklabel
set(handles.display,'YTick',ytick);
set(handles.display,'YTickLabel',signalLabel);
%set xtick and xticklabel
axis([start_index,end_index,ymin,ymax]);
xlabelList = start_index :(end_index-start_index)/5:end_index;
set(handles.display,'XTick',xlabelList);
xlabelList = xlabelList'./sampleRate;
xlabelLista = num2str(xlabelList);
set(handles.display,'XTickLabel',xlabelLista);
xlabel('Time (second)');

hold off


%% --- Get unique values from an array
function uniqueArr = uniqueVal(tmp)
if numel(tmp) > 0
    i = tmp(1);
    j = 1;
    uniqueArr(1) = i;
    k = 2;
    while i < tmp(numel(tmp))
       if(i ~= tmp(j))
           uniqueArr(k) = tmp(j);
           k = k + 1;
           i = tmp(j);
       end
       j = j + 1;
       i = i + 1;
    end
else
    uniqueArr = [];
end





%% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

s = strtrim(num2str(clock));
s = strcat('visualizeSignal_',s);
s = regexprep(s,'\W*','_');

imageName = strcat(s,'.fig');
saveas(handles.pushbutton3, imageName);
pause(1);

if exist(imageName,'file')
    disp('Screen Shot Successful!');
   return 
end

pause(2);

if exist(imageName,'file')
    disp('Screen Shot Success!');
   return 
end

disp('Screen Shot Fail!');
    
%% --- Executes on button press in loadFile.
function loadFile_Callback(hObject, eventdata, handles)
% hObject    handle to loadFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global sampleRate;
global channelNames;
[rawData,triggerSignal,sampleRate,channelNames,filterInfo,daqInfos]=loadSessionDataBin();




%storing data
global mat;
global trigMat;

mat = transpose(rawData);
trigMat = transpose(triggerSignal);


set(handles.start_index,'String','0');
set(handles.interval_length,'String','4');
scale_list = {'5','10','50','100','500','1000','10000'};
set(handles.scale_list,'String',scale_list);
set(handles.scale_list,'Value',4);
graphUpdate(handles);



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%
% --- Executes on selection change in Filter.
function Filter_Callback(hObject, eventdata, handles)
% hObject    handle to Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Filter contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Filter
global filterCoef;
global filterGrpDelay;
currentString = getCurrentPopupString(handles.Filter);

if strcmp(currentString,'no filter')
    get(handles.Filter,'Value');
    
elseif strcmp(currentString,'load a filter')%load a filter
        %get input file
       [filename, pathname] = uigetfile({'*.mat';'*.*'},'File Selector');
       %make sure user did select a input file
       if filename == 0
           set(handles.Filter,'Value',1);
           graphUpdate(handles);
           return
       end
       %load the file
       f = [pathname filename];
       loadFilter(f,handles);
end

graphUpdate(handles);

%%
function loadFilter(filterPath,handles)

    global filterCoef;
    global filterGrpDelay;
       %load the file
       f = filterPath;
       if exist(f,'file')
           filter = load(f);
       else
           return;
       end
       %get filter name
       filterNames = fieldnames(filter);
      
       %make sure the filter is not loaded
       a = get(handles.Filter,'String');
       doesExist = 0;
       for i = 1:numel(a)
           if strcmp(a(i,1),filterNames)
              doesExist = 1; 
              break;
           end
       end
       
       %append the filtername to the popup menu list
       if doesExist == 0
            a = [a;filterNames];
            i = numel(a);
            b = filter.(filterNames{1}).Num;
            filterCoef(1,i,:) = b;
            filterGrpDelay(1,i,1) = filter.(filterNames{1}).groupDelay;
       end
       
       set(handles.Filter,'String',a);
       %strsplit(a,'\n')
       set(handles.Filter,'Value',i);
       filterListFile = fopen('filterList.txt', 'a+');
       fprintf(filterListFile,f);
       fprintf(filterListFile,'\n');
       fclose(filterListFile);




%%
% --- Executes during object creation, after setting all properties.
function Filter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
