function varargout = sleepScore(varargin)
% SLEEPSCORE MATLAB code for sleepScore.fig
%      SLEEPSCORE, by itself, creates a new SLEEPSCORE or raises the existing
%      singleton*.
%
%      H = SLEEPSCORE returns the handle to a new SLEEPSCORE or the handle to
%      the existing singleton*.
%
%      SLEEPSCORE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SLEEPSCORE.M with the given input arguments.
%
%      SLEEPSCORE('Property','Value',...) creates a new SLEEPSCORE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sleepScore_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sleepScore_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sleepScore_OpeningFcn, ...
                   'gui_OutputFcn',  @sleepScore_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sleepScore is made visible.
function sleepScore_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sleepScore (see VARARGIN)

% Choose default command line output for sleepScore
handles.output = hObject;

% UIWAIT makes sleepScore wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%bring in necessary variables 
ePOCHS = evalin('base','ePOCHS');
segLength =  evalin('base','segLength');
assignin('base', 'k' , 1);
k = evalin('base','k');

% generate data
t = 1:segLength;
t = t.';
sig = ePOCHS{k,1};
sig = sig.';

% calculate shift
mi = min(sig,[],2);
ma = max(sig,[],2);
shift = cumsum([0; abs(ma(1:end-1))+ abs(mi(2:end))]);
shift = repmat(shift,1,4000);

%plot 'eeg' data
data = (sig+shift);
ePOCHS = evalin('base','ePOCHS');
handles.current_data = data;
handles.line = plot((t/1000), handles.current_data); %t in ms division turns to seconds 

f = gca;
a = gcf;
set(f, 'YTickLabel',{'Fp1','Fp2','F3','F4','Fz','Fc1','Fc2','Cz','P1','P2','C1','C2','Cp3','Cp4','P5','P6'});

 
% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = sleepScore_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%bring in necessary variables from workspace 
nuEpochs = evalin('base','nuEpochs');
nmEPOCHS = evalin('base','ePOCHS');
segLength =  evalin('base','segLength');
k = evalin('base','k');

%find k for next interval
if k >= 1 && k ~= nuEpochs
      k = k + 1;
      n = k;
else
    k = nuEpochs;
    n= k;
end

%change k
assignin('base', 'k', n);
k = evalin('base','k');

%data
t = 1:segLength;
t = t.';
sig = nmEPOCHS{k,1};
sig = sig.';

% calculate shift
mi = min(sig,[],2);
ma = max(sig,[],2);
shift = cumsum([0; abs(ma(1:end-1))+ abs(mi(2:end))]);
shift = repmat(shift,1,4000);

%plot 'eeg' data
data = (sig+shift);
nmEPOCHS = evalin('base','ePOCHS');
handles.current_data = data;

handles.line = plot((t/1000), handles.current_data);

f = gca;
a = gcf;
set(f, 'YTickLabel',{'Fp1','Fp2','F3','F4','Fz','Fc1','Fc2','Cz','P1','P2','C1','C2','Cp3','Cp4','P5','P6'});



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%bring in necessary variables from workspace 
nuEpochs = evalin('base','nuEpochs');
nmEPOCHS = evalin('base','ePOCHS');
segLength =  evalin('base','segLength');
k = evalin('base','k');

%find k for previous interval
if k > 1 && k ~= 1
      k = k - 1;
      n = k;
else
    k = 1;
    n=k;
end

%change k
assignin('base', 'k', n);
k = evalin('base','k');

%data
t = 1:segLength;
t = t.';
sig = nmEPOCHS{k,1};
sig = sig.';

% calculate shift
mi = min(sig,[],2);
ma = max(sig,[],2);
shift = cumsum([0; abs(ma(1:end-1))+ abs(mi(2:end))]);
shift = repmat(shift,1,4000);

%plot 'eeg' data

data = (sig+shift);
nmEPOCHS = evalin('base','ePOCHS');
handles.current_data = data;

handles.line = plot((t/1000), handles.current_data);
f = gca;
a = gcf;
set(f, 'YTickLabel',{'Fp1','Fp2','F3','F4','Fz','Fc1','Fc2','Cz','P1','P2','C1','C2','Cp3','Cp4','P5','P6'});



% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

k = evalin('base','k');

save('sleepyIntervals.txt','k','-ascii', '-append');  

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
k = evalin('base','k');

save('awakeIntervals.txt','k','-ascii', '-append');  


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

