%% Define the artifact rejection parameters
rawVoltageThreshold = (1E-07);
dataSegmentationLength = 1000;
channelNumber = 3;
bgEEG = 10000; % consider latency of 10-15 seconds or make this changeable

%% Load Data

% Retrieving file, may be handled in the GUI later, but useful to test here

% Loads file info from trial, **** requires the loadSessionData.m file in directory *****
[rawData,triggerSignal,sampleRate,channelNames,filterInfo,daqInfos]=loadSessionDataBin();

disp('Data is loaded');
load 'inputFilterCoef.mat';
[afterFrontendFilterData, afterFrontendFilterTrigger]=applyFrontendFilter(rawData,triggerSignal,frontendFilter);

%% Split continuous EEG data into epochs
%   This script does not take into account the start of the RSVP task. 
%   To start segemenation with respect to beginning of task, the correct RSVP trigger decoder will be needed.

[col, row] = size(afterFrontendFilterData);
endEEG = col; 

length = (endEEG - bgEEG); nuEpochs = floor(length/dataSegmentationLength); % number of epochs to be inspected 
lengthDivisable = (nuEpochs*dataSegmentationLength); % length necessary to have perfectly reshapable matrix 
start = abs(col-lengthDivisable+1);
EEG = afterFrontendFilterData(start:endEEG, :);

%% Windowing and Epoching

reformatEEG = cell2mat(num2cell(EEG)); ePOCHS = mat2cell(reformatEEG, repmat(dataSegmentationLength,nuEpochs,1), channelNumber); % creates a new matrix with nuEpochs containing segments of determined SegLength, referred to as ePOCHS
clear bgEEG endEEG start lengthDivisable col  

%% Find Noisy and Clean Epochs

% Creating empty arrays to append with noisy and clean intervals 
noise = [];
clean = [];

% Loop through the epochs created and check for any noise
for noiseI = 1:nuEpochs
% Get the averaged channel information for this epoch
    averagedChannelPerEpoch = mean(ePOCHS{noiseI}, 1);
    
% If the averaged channel information is +/- the raw voltage threshold,
%     append it to the noisy epoch
    if averagedChannelPerEpoch > rawVoltageThreshold
       noise = [noise noiseI];
    elseif averagedChannelPerEpoch < -rawVoltageThreshold
       noise = [noise noiseI];
    else
       clean = [clean noiseI];
    end
end


% Seperate good and bad arrays
% cleanArray = power(clean);
% badArray = power(~clean);

% Find how many intervals we have left to deal with 
[startclean, endclean] = size(clean);

% Display percent of clean data
cleanEnd = endclean; cleanPercentDisp = num2str(((cleanEnd/noiseI)*100));
disp([cleanPercentDisp,'% of your data is clean']);
 