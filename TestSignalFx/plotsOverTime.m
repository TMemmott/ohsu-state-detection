function [thetaFit, alphaFit, ebFit, medFit]  = plotsOverTime(thetaBand, alphaBand, blinkRates, medianOzFrequency)

%% Calculate Mean Measures for Experiment
    divisionLength = 4;
    %get the length of theta band
    lengthTheta = length(thetaBand);
    firstQuarter = floor(lengthTheta/divisionLength);
    n = firstQuarter; % average every n values
    a = cell2mat(thetaBand); % arbitrary data
    b = arrayfun(@(i) mean(a(i:i+n-1)),1:n:length(a)-n+1)'; % the averaged vector
    
%     % output overall mean
%     disp('Theta Mean:')
%     mean(a)
    % get the time range we want
    time = linspace(0, length(b), length(b));
    
    % output the fitted slope of these points
    thetaFit = polyfit(time, b', 1);
    %get the length of alpha band
    lengthAlpha = length(alphaBand);
    firstQuarter = floor(lengthAlpha/divisionLength);
    n = firstQuarter; % average every n values
    a = cell2mat(alphaBand); % arbitrary data
    b2 = arrayfun(@(i) mean(a(i:i+n-1)),1:n:length(a)-n+1)'; % the averaged vector
    
%     % output overall mean
%     disp('Alpha Mean:')
%     mean(a)
    
    % get the time range we want
    time = linspace(0, length(b2), length(b2));
    
    % output the fitted slope of these points
    alphaFit = polyfit(time, b2', 1);

    %get the length of blink rates
    lengthBR = length(blinkRates);
    firstQuarter = floor(lengthBR/divisionLength);
    n = firstQuarter; % average every n values
    a = blinkRates; % arbitrary data
    b3 = arrayfun(@(i) mean(a(i:i+n-1)),1:n:length(a)-n+1)'; % the averaged vector
%     
%     % output overall mean
%     disp('Eye-Blink Mean:')
%     mean(a)
    
    % get the time range we want
    time = linspace(0, length(b3), length(b3));
    
    % output the fitted slope of these points
    ebFit = polyfit(time, b3', 1);
    
%     time2 = linspace(0, length(blinkRates), length(blinkRates));
%     fit2 = polyfit(time2, blinkRates, 1);
    
    %get the length of median power
    lengthMedian = length(medianOzFrequency);
    firstQuarter = floor(lengthMedian/divisionLength);
    n = firstQuarter; % average every n values
    a = cell2mat(medianOzFrequency); % arbitrary data
    b4 = arrayfun(@(i) mean(a(i:i+n-1)),1:n:length(a)-n+1)'; % the averaged vector
    
        % get the time range we want
    time = linspace(0, length(b4), length(b4));
    
    % output the fitted slope of these points
    medFit = polyfit(time, b4', 1);
%     
%     % output overall mean
%     disp('Median Freq Mean:')
%     mean(a)

%     PLOTS:
    figure
    subplot(4,1,1)       % add first plot in 2 x 1 grid
    plot(b)
    title('Theta')

    subplot(4,1,2)       % add second plot in 2 x 1 grid
    plot(b2)       % plot using + markers
    title('Alpha')
    
    subplot(4,1,3)       % add second plot in 2 x 1 grid
    plot(b3)       % plot using + markers
    title('Eye-Blink Rate')
    
    subplot(4,1,4)       % add second plot in 2 x 1 grid
    plot(b4)       % plot using + markers
    title('Median Power')
    
end 