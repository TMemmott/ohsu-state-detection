function [ preHanningEPOCHS,RawEPOCHS, BlinkEpochs  ] = adaptiveEpocher( EEG,trialSampleTimeIndices,nuEpochs,segLength, lengthOfEpoch,afterFrontendFilterData )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
try
    %% 
    disp('Adaptive Epocher is ON. :)');
    % Channels f3, f4, c3, c4, p3, p4, o1, and o2
    RawEEG = EEG(:,[3, 5, 2, 6, 1, 7, 14, 15]);
    %find out the maximum lenth of the whole trial
    lengthTrial = max(size(trialSampleTimeIndices));
    TrialTimeIndex=1;
    StartOfEpoch(1,1:nuEpochs) = 0;
    EndOfEpoch(1,1:nuEpochs) = 0;
    for EpocherLoopIteration = 1:lengthTrial
        %Indexes the start and end of each epoch
        StartOfEpoch(EpocherLoopIteration)= trialSampleTimeIndices(TrialTimeIndex);
        EndOfEpoch(EpocherLoopIteration) = StartOfEpoch(EpocherLoopIteration)...
            + (segLength-1);

        % Here we make sure we don't go more than the number of trials we have
        if TrialTimeIndex <= (lengthTrial-lengthOfEpoch) 
           % Moving forward LengthOfEpoch inrements for the next epoch 
           TrialTimeIndex = TrialTimeIndex + lengthOfEpoch; 
        else
            break; %Get out of the loop once we have less than LengthOfEpoch samples to epoch
        end

    end
    %averaging P3 (column 1) with P4 (column 7) to get estimate of Pz 
    afterFrontendFilterData(:, 1)= (afterFrontendFilterData(:, 1) + afterFrontendFilterData(:, 7))/2;
    % Averaging O1 (coluumn 14) with O2 (column 15) to get estimate of
    % Oz
    afterFrontendFilterData(:, 14)= (afterFrontendFilterData(:, 14) + afterFrontendFilterData(:, 15))/2;
    
    preHanningEPOCHS = cell(1, nuEpochs);
    RawEPOCHS = cell(1,nuEpochs);
    BlinkEpochs = cell(1,nuEpochs);
    for i = 1: nuEpochs     
        if (StartOfEpoch(i)+segLength) <= max(size(afterFrontendFilterData))
            %Takes segLength long epochs from Filtered Data and puts them into a new matrix 
%                 called ePOCHS, in afterFrontendFilterData column 4 is Fz,
%                  column 8 is Cz, column 1 is estimate of Pz, and column
%                  14 is estimate of Oz
            preHanningEPOCHS{i} = afterFrontendFilterData(StartOfEpoch(i):EndOfEpoch(i), [4, 8, 1, 14]);
            % Channels f3, f4, c3, c4, p3, p4, o1, and o2
            RawEPOCHS{i} = afterFrontendFilterData(StartOfEpoch(i):EndOfEpoch(i), [3, 5, 2, 6, 1, 7, 14, 15]);
            BlinkEpochs{i} = afterFrontendFilterData(StartOfEpoch(i):EndOfEpoch(i), [10, 11]);
        else
             break; %Get out of the loop after the time stamp of the epoch is after the end of
%                  filtered data
        end     
    end
    
    
    
    catch exception
    
        disp(exception);

        keyboard;


end

