%% Eye Blink Calculations
function [eyeBlinkDrowsinessScore, blinkRateInitial, blinkRates]=eyeBlinkCalculation(Fp1, Fp2, bgEEG, fs)

try
    % Average to avoid false positives due to noise
    averageFps = (Fp1 + Fp2)/2;
    
    %  to-do screen for noise   

    % Inversing makes it easier to find peaks associated with blinks 
    % averageFps = (-averageFps);

    % Find the eye blink. Min peak distance and height defined here -- 
    [blinksAverage, locationAverage] = findpeaks(averageFps, 'MinPeakDistance', 75, 'MinPeakHeight', .00005);


    [sizeLocation, vv] = size(locationAverage); 

    % Calculate initial blink rate using countRateCalculation # of blinks, and
    % overall blink rate using the length of time elapsed and the number of blinks
    countRateCalculation = 10;
    for blink = 1:(sizeLocation - countRateCalculation)
        if locationAverage(blink) > bgEEG
            startLocation = blink;
            break
        end
    end

    blinkRateInitial = countRateCalculation / ((locationAverage(startLocation + countRateCalculation) - locationAverage(startLocation)) / fs); 

    blinkReducationIntervals = [];
    blinkRates = [];

    for bli = 1:(sizeLocation-countRateCalculation)
        % calculate a new blink rate for the next 10 blinks        
        newBlinkRate = countRateCalculation /((locationAverage(bli + countRateCalculation) - locationAverage(bli)) / fs);

        % Move forward by the count rate         
        bli = bli + countRateCalculation;
        blinkRates = [blinkRates newBlinkRate];
        % See if there is a change in blink rate      
        if newBlinkRate < blinkRateInitial
            blinkReducationIntervals = [blinkReducationIntervals bli];
        end
    end

    % To avoid just random error in blink detection, levels of drowsiness are used based on the percent of time blink reduction is seen in the file
    [~, blinkLength] = size(blinkReducationIntervals);
    
    percentChangeEyeBlink = blinkLength/bli;
    
    if percentChangeEyeBlink >= .30 && percentChangeEyeBlink <= .4499999999
        eyeBlinkDrowsinessScore = 1; 
    elseif percentChangeEyeBlink >= .45 && percentChangeEyeBlink <= .5999999999
        eyeBlinkDrowsinessScore = 2; 
    elseif percentChangeEyeBlink >= .6 && percentChangeEyeBlink <= .749999999
        eyeBlinkDrowsinessScore = 3; 
    elseif percentChangeEyeBlink >= .75 && percentChangeEyeBlink <= 1
        eyeBlinkDrowsinessScore = 4;
    else     
        eyeBlinkDrowsinessScore = 0; 
    end 


catch exception
    
    disp(exception);
    
    keyboard;
end