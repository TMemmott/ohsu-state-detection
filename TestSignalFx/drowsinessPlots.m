%**************************************************************************
%% 
% [ range ] = RawGraph( TransMatCleanRawEpochs, TransMatCleanBlinkEpochs );
    
% figure (3)% Channels f3, f4, c3, c4, p3, p4, o1, and o2
% ts = 1/fs*1000;
% tGraph = round(1:ts:8000,0);
% subplot(1,2,1)
% plot(tGraph,RawEEG(tGraph,1)), grid ON,...
%        xlabel('Time (sec)'), ylabel('Amplitude f3');
% subplot(1,2,2)
% plot(tGraph,RawEEG(tGraph,2)), grid ON,...
%         xlabel('time (sec)'), ylabel('Amplitude f4');
    
% invRawEEG = RawEEG';


% strips(rawData(160000:160500,[3,14]),10,4000/300,1), legend('f3', 'o1');
    
    
% figure(2)
% f3RawCleanEPOCHS = matCleanRawEPOCHS(:,1:8:(8*cleanEnd)-7);
% f4RawCleanEPOCHS = matCleanRawEPOCHS(:,2:8:(8*cleanEnd)-6);
% c3RawCleanEPOCHS = matCleanRawEPOCHS(:,3:8:(8*cleanEnd)-5);
% c4RawCleanEPOCHS = matCleanRawEPOCHS(:,4:8:(8*cleanEnd)-4);
% p3RawCleanEPOCHS = matCleanRawEPOCHS(:,5:8:(8*cleanEnd)-3);
% p4RawCleanEPOCHS = matCleanRawEPOCHS(:,6:8:(8*cleanEnd)-2);
% o1RawCleanEPOCHS = matCleanRawEPOCHS(:,7:8:(8*cleanEnd)-1);
% o2RawCleanEPOCHS = matCleanRawEPOCHS(:,8:8:(8*cleanEnd));
% 
% MeanOfF3RawCleanEPOCHS = mean(f3RawCleanEPOCHS(:,1:2),2);
% MeanOfF4RawCleanEPOCHS = mean(f4RawCleanEPOCHS(:,1:2),2);
% MeanOfC3RawCleanEPOCHS = mean(c3RawCleanEPOCHS(:,1:2),2);
% MeanOfC4RawCleanEPOCHS = mean(c4RawCleanEPOCHS(:,1:2),2);
% MeanOfP3RawCleanEPOCHS = mean(p3RawCleanEPOCHS(:,1:2),2);
% MeanOfP4RawCleanEPOCHS = mean(p4RawCleanEPOCHS(:,1:2),2);
% MeanOfO1RawCleanEPOCHS = mean(o1RawCleanEPOCHS(:,1:2),2);
% MeanOfO2RawCleanEPOCHS = mean(o2RawCleanEPOCHS(:,1:2),2);
% MeanOfRawCleanEPOCHSLeft = [MeanOfF3RawCleanEPOCHS MeanOfC3RawCleanEPOCHS...
%            MeanOfP3RawCleanEPOCHS MeanOfO1RawCleanEPOCHS]*10^6;
% MeanOfRawCleanEPOCHSRight = [MeanOfF4RawCleanEPOCHS MeanOfC4RawCleanEPOCHS...
%            MeanOfP4RawCleanEPOCHS MeanOfO2RawCleanEPOCHS]*10^6;
   %axis([1 40000 -30 40]),... legend('f3', 'c3','p3', 'o1'), ...   
% subplot(1,2,1)
% strips(MeanOfRawCleanEPOCHSLeft(:,1),1,300), grid ON,...
%        xlabel('Time (sec)'), ylabel('Amplitude f3');
% subplot(1,2,2)
% strips(MeanOfRawCleanEPOCHSRight(:,1),1,300), grid ON,...
%         xlabel('time (sec)'), ylabel('Amplitude f4');
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
% figure(4);% The graph for Drowsiness score. [Awake Light D and Deep D]' in 
% % time and frequency domains averaged over epochs. 
% 
% % add an if statment
% awakeCleanEPOCHS = cleanEPOCHS(arousal);
% LightDrowsyCleanEPOCHS = cleanEPOCHS(lightDrowsy);
% DeepDrowsyCleanEPOCHS = cleanEPOCHS(deepDrowsy);
% 
% matAwakeCleanEPOCHS = cell2mat(awakeCleanEPOCHS);
% matLightDrowsyCleanEPOCHS = cell2mat(LightDrowsyCleanEPOCHS);
% matDeepDrowsyCleanEPOCHS = cell2mat(DeepDrowsyCleanEPOCHS);
% plotFrequency = 0:fs/N:fs;
% plotTime = round(1./(plotFrequency(28:268))*1000,0);
% 
% %Find mean of each mat...CleanEPOCHS
% AvgAwakeCleanEPOCHS = mean(matAwakeCleanEPOCHS,2);
% % AvgLightDrowsyCleanEPOCHS = 
% 
% % % For awake epochs
% if isempty(arousal)==0
%     
%     FzAwakeCleanEPOCHS = matAwakeCleanEPOCHS(:,1:4:(4*max(size(arousal))-3));
%     CzAwakeCleanEPOCHS = matAwakeCleanEPOCHS(:,2:4:(4*max(size(arousal))-2));
%     PzAwakeCleanEPOCHS = matAwakeCleanEPOCHS(:,3:4:(4*max(size(arousal))-1));
%     OzAwakeCleanEPOCHS = matAwakeCleanEPOCHS(:,4:4:(4*max(size(arousal))));
% 
%     MeanOfFzAwakeCleanEPOCHS = mean(FzAwakeCleanEPOCHS(:,1:max(size(arousal))),2);
%     MeanOfCzAwakeCleanEPOCHS = mean(CzAwakeCleanEPOCHS(:,1:max(size(arousal))),2);
%     MeanOfPzAwakeCleanEPOCHS = mean(PzAwakeCleanEPOCHS(:,1:max(size(arousal))),2);
%     MeanOfOzAwakeCleanEPOCHS = mean(OzAwakeCleanEPOCHS(:,1:max(size(arousal))),2);
%     MeanOfAwakeCleanEPOCHS = [MeanOfFzAwakeCleanEPOCHS MeanOfCzAwakeCleanEPOCHS...
%          MeanOfPzAwakeCleanEPOCHS  MeanOfOzAwakeCleanEPOCHS]*10^6;
%     
%     subplot(3,1,1)
%     plot(plotFrequency(28:268),MeanOfAwakeCleanEPOCHS(plotTime,:)), grid ON,...
%          legend('Fz','Cz','Pz', 'Oz'), ...
%         xlabel('Freq (Hz)'), ylabel('arousal Pow (uV^2)');
% else
%     disp('There was no arousal epochs to graph!');
% end
% 
% %For light drowsy epochs
% FzLightDrowsyCleanEPOCHS = matLightDrowsyCleanEPOCHS(:,1:4:(4*max(size(lightDrowsy))-3));
% CzLightDrowsyCleanEPOCHS = matLightDrowsyCleanEPOCHS(:,2:4:(4*max(size(lightDrowsy))-2));
% PzLightDrowsyCleanEPOCHS = matLightDrowsyCleanEPOCHS(:,3:4:(4*max(size(lightDrowsy))-1));
% OzLightDrowsyCleanEPOCHS = matLightDrowsyCleanEPOCHS(:,4:4:(4*max(size(lightDrowsy))));
% 
% MeanOfFzLightDrowsyCleanEPOCHS = mean(FzLightDrowsyCleanEPOCHS(:,1:max(size(lightDrowsy))),2);
% MeanOfCzLightDrowsyCleanEPOCHS = mean(CzLightDrowsyCleanEPOCHS(:,1:max(size(lightDrowsy))),2);
% MeanOfPzLightDrowsyCleanEPOCHS = mean(PzLightDrowsyCleanEPOCHS(:,1:max(size(lightDrowsy))),2);
% MeanOfOzLightDrowsyCleanEPOCHS = mean(OzLightDrowsyCleanEPOCHS(:,1:max(size(lightDrowsy))),2);
% MeanOfLightDrowsyCleanEPOCHS = [MeanOfFzLightDrowsyCleanEPOCHS MeanOfCzLightDrowsyCleanEPOCHS...
%      MeanOfPzLightDrowsyCleanEPOCHS  MeanOfOzLightDrowsyCleanEPOCHS]*10^6;
% subplot(3,1,2)
% plot(plotFrequency(28:268),MeanOfLightDrowsyCleanEPOCHS(plotTime,:)), grid ON,...
%     legend('Fz','Cz','Pz', 'Oz'), ...
%     xlabel('Freq (Hz)'), ylabel('Light Drowsy Pow (uV^2)');
% %  axis([1 20 -0.1 0.1]),
% 
% %For Deep drowsy epochs
% if isempty(deepDrowsy)==0
% 
%     FzDeepDrowsyCleanEPOCHS = matDeepDrowsyCleanEPOCHS(:,1:4:(4*max(size(deepDrowsy))-3));
%     CzDeepDrowsyCleanEPOCHS = matDeepDrowsyCleanEPOCHS(:,2:4:(4*max(size(deepDrowsy))-2));
%     PzDeepDrowsyCleanEPOCHS = matDeepDrowsyCleanEPOCHS(:,3:4:(4*max(size(deepDrowsy))-1));
%     OzDeepDrowsyCleanEPOCHS = matDeepDrowsyCleanEPOCHS(:,4:4:(4*max(size(deepDrowsy))));
% 
%     MeanOfFzDeepDrowsyCleanEPOCHS = mean(FzDeepDrowsyCleanEPOCHS(:,1:max(size(deepDrowsy))),2);
%     MeanOfCzDeepDrowsyCleanEPOCHS = mean(CzDeepDrowsyCleanEPOCHS(:,1:max(size(deepDrowsy))),2);
%     MeanOfPzDeepDrowsyCleanEPOCHS = mean(PzDeepDrowsyCleanEPOCHS(:,1:max(size(deepDrowsy))),2);
%     MeanOfOzDeepDrowsyCleanEPOCHS = mean(OzDeepDrowsyCleanEPOCHS(:,1:max(size(deepDrowsy))),2);
%     MeanOfDeepDrowsyCleanEPOCHS = [MeanOfFzDeepDrowsyCleanEPOCHS MeanOfCzDeepDrowsyCleanEPOCHS...
%          MeanOfPzDeepDrowsyCleanEPOCHS  MeanOfOzDeepDrowsyCleanEPOCHS]*10^6;
%     subplot(3,1,3)
%     plot(plotFrequency(28:268),MeanOfDeepDrowsyCleanEPOCHS(plotTime,:)), grid ON,...
%          legend('Fz','Cz','Pz', 'Oz'), ...
%         xlabel('Freq (Hz)'), ylabel('Deep Drowsy Pow (uV^2)');
% else
%     disp('There was no deep drowsy epochs to graph!');
% end
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% subplot(3,2,1)
% plot(FunFirstThirtyTwoSec*10^6,'-'), xlabel('Time (msec)'), ylabel('1st Epoch (uV)'),
%     grid ON, legend('Fz','Cz','Pz', 'Oz'),title('EPOCH Graph');
%     
% middleRange = round(endclean/2):(round(endclean/2)+thirtyTwoSec);
% matEPOCHS = cleanEPOCHS{1,middleRange};
% 
% subplot(3,2,3)
% plot(FunFirstThirtyTwoSec*10^6,'-'), xlabel('Time (msec)'), ylabel('Middle (uV)'),...
%     grid ON, legend('Fz','Cz','Pz', 'Oz');
% matEPOCHS = cleanEPOCHS{1,endclean};
% subplot(3,2,5)
% plot(FunFirstThirtyTwoSec*10^6,'-'), xlabel('Time (msec)'), ylabel('Last (uV)'),...
%     grid ON, legend('Fz','Cz','Pz', 'Oz');
% 
% frequency = 1.50:fs/N:fs/15; 
% xdftEpochFirst = cleanDataFFT{1,1};
% xdftEpochFirst = xdftEpochFirst(1:thirteenHz,:);
% matPowerEpochFirst = (1/(fs*N))*abs(xdftEpochFirst).^2;
% matPowerEpochFirst(2:end-1,:) = matPowerEpochFirst(2:end-1,:);
% matPowerEpochFirst = (2*10^12)*matPowerEpochFirst;
% subplot(3,2,2)
% median_freq = medfreq(power{1,1},freq);
% [median_freq, median_freq],[0,max(mat_power(:,1))]
% % TODO: multiply by *10^6 the y-axis
% issue with axis scaling: axis([0 50 -100 -170]),
% hold on
% plot(frequency, matPowerEpochFirst,'linewidth', 1), grid ON,...
%     legend('Fz','Cz','Pz', 'Oz'), xlabel('Freq (Hz)'),...
%     ylabel('1st (uV^2)/Hz'), title('Power Graph');
% hold off % , axis([8 13 0 30])
% 
% MiddleEpoch = round(cleanEnd/2);  
% xdftEpochMiddle = cleanDataFFT{1,MiddleEpoch};
% xdftEpochMiddle = xdftEpochMiddle(1:thirteenHz,:);
% matPowerEpochMiddle = (1/(fs*N))*abs(xdftEpochMiddle).^2;
% matPowerEpochMiddle(2:end-1,:) = matPowerEpochMiddle(2:end-1,:);
% matPowerEpochMiddle = (2*10^12)*matPowerEpochMiddle;
% subplot(3,2,4)
% median_freq = medfreq(power{1,24},freq);
% hold on
% plot(frequency, matPowerEpochMiddle,'linewidth', 1),grid ON,...
%     legend('Fz','Cz','Pz', 'Oz'),...
%     xlabel('Freq (Hz)'), ylabel('Middle (uV^2)/Hz');
% hold off
% 
% EndClassifiedEpoch = drowsyTest-1;
% xdftEpochLast = cleanDataFFT{1,EndClassifiedEpoch};
% xdftEpochLast = xdftEpochLast(1:thirteenHz,:);
% matPowerEpochLast = (1/(fs*N))*abs(xdftEpochLast).^2;
% matPowerEpochLast(2:end-1,:) = matPowerEpochLast(2:end-1,:);
% matPowerEpochLast = (2*10^12)*matPowerEpochLast;
% subplot(3,2,6)
% median_freq = medfreq(power{1,48},freq);
% hold on
% plot(frequency, matPowerEpochLast, 'linewidth', 1),grid ON,...
%     legend('Fz','Cz','Pz', 'Oz'),...
%     xlabel('Freq (Hz)'), ylabel('Last (uV^2)/Hz');
% hold off
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% figure(3);

% FirstThirtyTwoSec = cleanEPOCHS(1:thirtyTwoSec);
% matFirstThirtyTwoSec = cell2mat(FirstThirtyTwoSec);
% OzFirstThirtyTwoSec = matFirstThirtyTwoSec(:,(1:4:(4*thirtyTwoSec)));
% PzFirstThirtyTwoSec = matFirstThirtyTwoSec(:,1:4:(4*thirtyTwoSec-1));
% CzFirstThirtyTwoSec = matFirstThirtyTwoSec(:,1:4:(4*thirtyTwoSec-2));
% FzFirstThirtyTwoSec = matFirstThirtyTwoSec(:,1:4:(4*thirtyTwoSec-3));
% 
% avgOzFirstThirtyTwoSec = mean(OzFirstThirtyTwoSec(:,(1:thirtyTwoSec)),2);
% avgPzFirstThirtyTwoSec = mean(PzFirstThirtyTwoSec(:,(1:thirtyTwoSec)),2);
% avgCzFirstThirtyTwoSec = mean(CzFirstThirtyTwoSec(:,(1:thirtyTwoSec)),2);
% avgFzFirstThirtyTwoSec = mean(FzFirstThirtyTwoSec(:,(1:thirtyTwoSec)),2);
% FunFirstThirtyTwoSec = [avgFzFirstThirtyTwoSec,avgCzFirstThirtyTwoSec,...
%     avgPzFirstThirtyTwoSec,avgOzFirstThirtyTwoSec]; 

% 
% % frequency = 0:fs/N:fs/10;
% matPowerFull = cell2mat(cleanArray(1,:));
% matPowerFull = matPowerFull*10^6;
% % plot(frequency,matPowerFull(1:401,:)),legend('Ep 1','Ep 2','Ep 3'),grid;
% % semilogy(frequency,matPowerFull(1:401,:)),legend('Ep 1','Ep 2','Ep 3'),grid;
% %     
% % make a column in matPowerFull for coressponding frequencies. So when
% % graphing we can easil see the coressponding frequency
% 
% % timeBf = 1:segLength;
% % freqBf= 1./timeBf;
% % matPowerFull = [freqBf;matPowerFull];
% % EpochNumbers= 1:cleanEnd;
% % PowerAmplitudes = matPowerFull(timeBf,EpochNumbers);
% % y = PowerAmplitudes(:,);
% % z = PowerAmplitudes[,:];
% % 
% figure(5);
% % Color = gradient();
% mesh(matPowerFull(plotTime,1:cleanEnd)),axis([0 cleanEnd 46 251 0 80]),...
%     xlabel('Epochs'), ylabel('Time (msec)'), zlabel('(uV^2)/Hz');
% 
% % surf(matPowerFull((251:-1:46),:)),axis([0 48 46 251 0 100]),...
% %     xlabel('Epochs'), ylabel('Time (msec)'), zlabel('(uV^2)/Hz');
% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% GraphCleanArray = cell2mat(cleanArray(1,:));
% % window = [0:0.25:29.75];
% % figure(5);
% % pwelch(GraphCleanArray((251:-1:1),:));% ,axis([0.14 0.79 -145 -105])
% % pyulear(GraphCleanArray((1:251),:), 4), xlabel('Freq 4-22 Hz Normalized by pi '),axis([0.14 0.79 -140 -100]);
% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% figure(6);
% boxplot(OzCleanEPOCHS), xlabel('Epochs'),...
%     ylabel('Power (uV^2)/Hz'), title('Median Freq in Power');
% % 
% % figure(7);
% % boxplot(OzAwakeEPOCHS, awake), xlabel('Awake Epochs'),...
% %     ylabel('Power (uV^2)/Hz'), title('Median Freq in Power');
% % 
% % figure(8);
% % boxplot(OzDeepDrowsyEPOCHS, deepDrowsy), xlabel('Deep Drowsy Epochs'),...
% %     ylabel('Power (uV^2)/Hz'), title('Median Freq in Power');
% % %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% 
% 
% % figure(9)
% % medfreq(OzCleanEPOCHS(:,[1,round(cleanEnd/2),cleanEnd]),fs),legend ('1st Ep','Middle Ep','Last Ep')...
% %     , axis([1.5 20 -160 -95]);
% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% % Summery Table 
% 
% 
% tableMeanOfStartOfFzAntAlphaBand = round(MeanOfStartOfFzAntAlphaBand *(10^6),2);
% tableMeanOfStartOfMedianOzFrequencies = round(MeanOfStartOfMedianOzFrequencies,2);
% 
% drowsinessRows = {'Epochs','ThetaOverAlpha_uV2','State','FzAlphaBandPow_uV2', ...
%     'AntOfAlpha', 'OzMedFreq_Hz', 'MedFreqState'};
% 
% mentalState = cleanArray(2,:);
% anteriorizationState = AntCleanArray(2,:);
% medianFreqState = cleanMedianOzFrequency(2,:);
% 
% matAlphaBand = cell2mat(alphaBand);
% matThetaBand = cell2mat(thetaBand);
% 
% matThetaOverAlphaBand = round((matThetaBand./matAlphaBand),2);
% tableFzAntAlphaBand = round(10^6*FzAntAlphaBand',2);
% matCleanMedianOzFrequency = round(cell2mat(cleanMedianOzFrequency(1,:)'),2);
% 
% Drowsiness_results = {clean' matThetaOverAlphaBand' mentalState' ...
%     tableFzAntAlphaBand, anteriorizationState' matCleanMedianOzFrequency ...
%     medianFreqState'};
% 
% drowsinessTable = table(Drowsiness_results{1,1},Drowsiness_results{1,2},...
%     Drowsiness_results{1,3},Drowsiness_results{1,4}, ...
%     Drowsiness_results{1,5},Drowsiness_results{1,6},Drowsiness_results{1,7}...
%     ,'VariableNames', drowsinessRows)
% 
% fprintf('Mean of 1st 32sec Theta/Alpha: %g \n',meanThetaOverAlphaBand);
% fprintf('Mean of 1st 32sec Fz Alpha Band Pow: %g (uV^2) \n',tableMeanOfStartOfFzAntAlphaBand);
% fprintf('Mean of 1st 32sec Oz Median Freq: %g (uV^2) \n',tableMeanOfStartOfMedianOzFrequencies);
% fprintf('Blink Rate Initial: %g \n',blinkRateInitial);
% % 
% % %         table_figure = uitable(figure);
% % %         table_figure.ColumnName = {'Epochs' 'State' 'AnteriorizationOfAlpha'};
% % %         table_figure.Data = {Drowsiness_results{1,2},Drowsiness_results{1,3}};
% %  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% % % TODO: make a graph of non-drowsy epochs here
% % %     figure(5);
% % %     surf(mat_power_full(frequency,[1:end])),...
% % %         xlabel('Epochs'), ylabel('Freq (Hz)'), zlabel('(uV^2)/Hz'); 
