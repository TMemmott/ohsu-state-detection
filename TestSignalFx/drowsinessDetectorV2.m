%  ______   ___   ___   ______   __  __       ______   _________  ________   _________  ______       
% /_____/\ /__/\ /__/\ /_____/\ /_/\/_/\     /_____/\ /________/\/_______/\ /________/\/_____/\     
% \:::_ \ \\::\ \\  \ \\::::_\/_\:\ \:\ \    \::::_\/_\__.::.__\/\::: _  \ \\__.::.__\/\::::_\/_    
%  \:\ \ \ \\::\/_\ .\ \\:\/___/\\:\ \:\ \    \:\/___/\  \::\ \   \::(_)  \ \  \::\ \   \:\/___/\   
%   \:\ \ \ \\:: ___::\ \\_::._\:\\:\ \:\ \    \_::._\:\  \::\ \   \:: __  \ \  \::\ \   \::___\/_   
%    \:\_\ \ \\: \ \\::\ \ /____\:\\:\_\:\ \     /____\:\  \::\ \   \:.\ \  \ \  \::\ \   \:\____/\  
%     \_____\/ \__\/ \::\/ \_____\/ \_____\/     \_____\/   \__\/    \__\/\__\/   \__\/    \_____\/                                                                                                                                                                                   
%   ______   ______   _________  ______   ______  _________  ______   ______       
%  /_____/\ /_____/\ /________/\/_____/\ /_____/\/________/\/_____/\ /_____/\      
%  \:::_ \ \\::::_\/_\__.::.__\/\::::_\/_\:::__\/\__.::.__\/\:::_ \ \\:::_ \ \     
%   \:\ \ \ \\:\/___/\  \::\ \   \:\/___/\\:\ \  __ \::\ \   \:\ \ \ \\:(_) ) )_   
%    \:\ \ \ \\::___\/_  \::\ \   \::___\/_\:\ \/_/\ \::\ \   \:\ \ \ \\: __ `\ \  
%     \:\/.:| |\:\____/\  \::\ \   \:\____/\\:\_\ \ \ \::\ \   \:\_\ \ \\ \ `\ \ \ 
%      \____/_/ \_____\/   \__\/    \_____\/ \_____\/  \__\/    \_____\/ \_\/ \_\/ 






% Created by. Tab Memmott & Sina Dabiri at OHSU under the supervision of Dr. Barry Oken. 

% Corresponding email: memmott@ohsu.edu

% This script loads in an EEG file and outputs a score indicating the user's
%     state or drowsiness level.

% It has several features that may be used: 
%   I. Eye blink rate calculation 
%   II. PSD analysis
%   III. Adaptive Epoching
%   IV. Individualized Weighting
%   V. Median Power Frequency
%   VI. Anteriorization of Alpha Analysis


% Loads file info from trial, **** requires the loadSessionData.m file in directory *****
[rawData,triggerSignal,fs,channelNames,filterInfo,daqInfos,sessionFolder]=loadSessionDataBin();
addpath(genpath(sessionFolder));


%% Define EEG and Drowsiness parameters 

% get drowsiness parameters
[segLength, adaptiveEpocherEnabled, alternateWeightingEnabled, medication, ...
    anteriorizationOfAlpha, MedianFrequencyOfPower, lengthOfEpoch, eightSec, ...
    thirtyTwoSec, thirteenHz, channelNumber] = getDrowsinessParameters();

%% Windowing and Epoching

 [afterFrontendFilterData, trialSampleTimeIndices, participantName, participantTrial, EEG, nuEpochs, freq, bgEEG] = setUpRSVPData(rawData,triggerSignal, sessionFolder, fs, segLength);

[p3, c3, f3, fz, f4, c4, ...
 p4, cz, a1, Fp1, Fp2, t3, t5, ...
 o1, o2, f7, f8, a2, t6, t4] = getChannels(EEG);

% DSI correction
% p1 = p3; 
% p2 = p4; 

% Calculation of drowsiness score switch
switch adaptiveEpocherEnabled
    case false
        EEG = EEG(:,[4, 8]); %NOTE: if changing this, must change channel number Fz, Cz
        reformatEEG = cell2mat(num2cell(EEG)); 
        preHanningEPOCHS = mat2cell(reformatEEG, repmat(segLength,nuEpochs,1), channelNumber); 
        % creates a new matrix with nuEpochs containing segments of determined SegLength, referred to as ePOCHS
        % clear endEEG start lengthDivisable col  % deletes unnecessary files in workspace 

        reformatEEG = EEG;

    otherwise  %If adaptive epocher is chosen, then
        
        [ preHanningEPOCHS,RawEPOCHS, BlinkEpochs  ] =...
            adaptiveEpocher( EEG,trialSampleTimeIndices,nuEpochs,segLength,...
            lengthOfEpoch,afterFrontendFilterData );
      
end

% calculate a hanning window
hanning = hann(segLength, 'periodic');
Windowing = repmat(hanning,1, channelNumber);

matPreHanningEPOCHS = cell2mat(preHanningEPOCHS');
% EEG = EEG.*hanning; This is for applying hanning to whole data set

pzEST = (p1 + p2)/2; 

disp(' . ');

%% Runs FFT in sequence on the created ePOCHS of length segLength

[windowedEpochs, dataFFT, meanDataFFT, power, anteriorPower, iPower,...
    medianOzFrequency] = calculateFrequencyMeasures(nuEpochs,...
    Windowing, preHanningEPOCHS, segLength, fs, channelNumber);


%% Takes bands of defined frequencies for Noise Calculation

[clean, cleanEnd] = rejectSequencesByFreq(nuEpochs, power, iPower,...
    freq, participantName, participantTrial);

cleanArray = power(clean);%CleanArray contains noiseless epochs's powers
AntCleanArray = anteriorPower(clean); %AntCleanArray contains the noiseless 
% epoch's power for all the channels

cleanMedianOzFrequency = medianOzFrequency(clean);%cleanMedianOzFrequency
% contains the power of the median frequency in each epoch for Oz channel
cleanEPOCHS = windowedEpochs(clean);
matCleanEPOCHS = cell2mat(cleanEPOCHS);
% OzCleanEPOCHS = matCleanEPOCHS(:,(1:4:(4*cleanEnd)));%OzCleanEPOCHS contains
%the signal in Oz channel 
cleanDataFFT = dataFFT(clean);
badArray = power(~clean);


%% Eye Blink Calculations

[eyeBlinkDrowsinessScore, blinkRateInitial, blinkRates] = eyeBlinkCalculation(Fp1, Fp2, bgEEG, fs);

%% Power and Starting Value Calculations

[totalPower, deltaBand, thetaBand, alphaBand, ...
    anteriorAlphaBand, beta1Band, beta2Band] = calculatePowerBands(cleanArray, AntCleanArray, cleanEnd, freq);


startingTheta = (thetaBand{1} + thetaBand{2} + thetaBand{3})/3; % looks for a 15% increase in theta
startingAlpha = (alphaBand{1} + alphaBand{2} + alphaBand{3})/3; % looks for a 15% decrease in alpha


%% Calculate Drowsiness Score

cleanEnd2 = cleanEnd / eightSec;

switch alternateWeightingEnabled
    case false
        
        % get frequency based drowsiness scores        
        [freqDrowsinessScoreAlpha, ...
         freqDrowsinessScoreTheta] = getFreqDrowsinessScores(cleanEnd, startingTheta, thetaBand, startingAlpha, alphaBand);
     
 
        if anteriorizationOfAlpha == true
        % Check for anteriorization of alpha
            disp('Anteriorization of Alpha check is ON! ');
            [FzAntAlphaBand, MeanOfStartOfFzAntAlphaBand, AntCleanArray,...
                antAlpha8sec, antAlphaFz, NoAntAlpha,...
                AnteriorizationOfAlphaScore ] = AntOfAlphaScore(...
                anteriorAlphaBand, cleanEnd, eightSec, thirtyTwoSec,...
                cleanEnd2, AntCleanArray );
            fprintf('Anteriorization Score: %d \n', AnteriorizationOfAlphaScore);
        end
        
        % Median Power Frequency
        if MedianFrequencyOfPower == true
            [ cleanMedianOzFrequency, medianFreqDrowsinessScore, MeanOfStartOfMedianOzFrequencies, DecreaseOzMedPowFreq,...
                EightsecDecreaseOzMedPowFreq, NoDecOzMedPowFreq,...
                DecreaseMedianFreqScore] = MedFreqOfPow(...
                cleanMedianOzFrequency, cleanEnd, eightSec, thirtyTwoSec, cleanEnd2 );
            fprintf('Median Frequency Score: %d \n', DecreaseMedianFreqScore);
            fprintf('Median Frequency Score2: %d \n', medianFreqDrowsinessScore);
        end
        
        % print scores        
            % fprintf('Drowsiness Score: %s \n', drowsinessScore);
        fprintf('Frequency Theta Score: %d \n', freqDrowsinessScoreTheta);
        fprintf('Frequency Alpha Score: %d \n', freqDrowsinessScoreAlpha);
        fprintf('Eye Blink Rate Score: %x \n', eyeBlinkDrowsinessScore);

    otherwise %Using the Alternating Weighting for eye blink and mental states.
                       
        disp('Alternating Weighting is ON. :)');
        
        cleanRawEPOCHS = RawEPOCHS(clean);
        CleanBlinkEpochs = BlinkEpochs(clean);
         
        matCleanRawEPOCHS = cell2mat(cleanRawEPOCHS);
        TransMatCleanRawEpochs = cell2mat(cleanRawEPOCHS');
        TransMatCleanBlinkEpochs = cell2mat(CleanBlinkEpochs');
        
        % I think this scoring is off and should be reconsidered      
        [meanThetaOverAlphaBand, cleanArray, lightDrowsy,...
                deepDrowsy, arousal, awake, cleanEnd2, freqDrowsinessScore ]...
                = drowsyScore( cleanEnd, beta1Band, alphaBand, thetaBand, ...
                eightSec, thirtyTwoSec, cleanArray );
            

        if anteriorizationOfAlpha == true
        % Check for anteriorization of alpha:
            disp('Anteriorization of Alpha check is ON! ');
            [FzAntAlphaBand, MeanOfStartOfFzAntAlphaBand, AntCleanArray,...
                antAlpha8sec, antAlphaFz, NoAntAlpha,...
                AnteriorizationOfAlphaScore ] = AntOfAlphaScore(...
                anteriorAlphaBand, cleanEnd, eightSec, thirtyTwoSec,...
                cleanEnd2, AntCleanArray );
          
            if medication == true
                %Subject is taking EEG altering medication
                disp('Subject is taking medication. ');%Eye blink score will be dominant.

                drowsinessScore = num2str((eyeBlinkDrowsinessScore*0.60 + freqDrowsinessScore*.40)); 
            else
                drowsinessScore = num2str((eyeBlinkDrowsinessScore*0.50 + freqDrowsinessScore*.50)); 
            end
        else
            if medication == true
                drowsinessScore = num2str((eyeBlinkDrowsinessScore*0.70 + freqDrowsinessScore*.30));
            else
                drowsinessScore = num2str((eyeBlinkDrowsinessScore*0.60 + freqDrowsinessScore*.40)); 
            end
        end 
            
    

    %Median Power Frequency
    if MedianFrequencyOfPower == true
        [ cleanMedianOzFrequency, MeanOfStartOfMedianOzFrequencies, DecreaseOzMedPowFreq,...
            EightsecDecreaseOzMedPowFreq, NoDecOzMedPowFreq,...
            DecreaseMedianFreqScore] = MedFreqOfPow(...
            cleanMedianOzFrequency, cleanEnd, eightSec, thirtyTwoSec, cleanEnd2 );
    end
    
    AwakeOzEPOCHS = cleanEPOCHS(awake);
    matAwakeOzEPOCHS = cell2mat(AwakeOzEPOCHS);
    [~, AwakeEnd] = size(AwakeOzEPOCHS); % how to incorporate drowsy and 
    % anteriorization awake epochs? 
    OzAwakeEPOCHS = matAwakeOzEPOCHS(:,(1:4:(4*AwakeEnd)));

    DeepDrowsyOzEPOCHS = cleanEPOCHS(deepDrowsy);
    matDeepDrowsyOzEPOCHS = cell2mat(DeepDrowsyOzEPOCHS);
    [~, DeepDrowsyEnd] = size(DeepDrowsyOzEPOCHS); % how to incorporate drowsy and 
    % anteriorization awake epochs? 
    OzDeepDrowsyEPOCHS = matDeepDrowsyOzEPOCHS(:,(1:4:(4*DeepDrowsyEnd)));
                
end %end of Alternating weighting script


[thetaFit, alphaFit, ebFit, medFit]  = plotsOverTime(thetaBand, alphaBand, blinkRates, medianOzFrequency)