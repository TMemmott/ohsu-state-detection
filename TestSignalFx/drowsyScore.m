function [ meanThetaOverAlphaBand, cleanArray, lightDrowsy, deepDrowsy, arousal, awake, cleanEnd2, freqDrowsinessScore ] = drowsyScore( cleanEnd, beta1Band, alphaBand, thetaBand, eightSec, thirtyTwoSec, cleanArray )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
try
    %% 
    %Initializing vaules as zero.
    stateChange = 0; 
    drowsinessScore = 0;
    counter = 0;

    %Calculating the mean for the first 32 sec
    %First we will convert the first 32 sec into matrix format
    benchBeta1Band32sec = cell(1,thirtyTwoSec);
    benchAlphaBand32sec = cell(1,thirtyTwoSec);
    benchThetaBand32sec = cell(1,thirtyTwoSec);
    for benchmark=1:thirtyTwoSec
        benchBeta1Band32sec{benchmark} = beta1Band{benchmark};
        benchAlphaBand32sec{benchmark} = alphaBand{benchmark};
        benchThetaBand32sec{benchmark} = thetaBand{benchmark};
    end

    meanOfStartBeta1Pow  = mean(cell2mat(benchBeta1Band32sec));
    meanOfStartAlphaPow  = mean(cell2mat(benchAlphaBand32sec));
    meanOfStartThetaPow  = mean(cell2mat(benchThetaBand32sec));
    meanThetaOverAlphaBand = round(meanOfStartThetaPow/meanOfStartAlphaPow,2);

    awake = [];
    lightDrowsy = [];
    deepDrowsy = [];
    arousal = [];
    beta1BandCurrent = cell(1,cleanEnd);
    alphaBandCurrent = cell(1,cleanEnd);
    thetaBandCurrent = cell(1,cleanEnd);
    cleanEnd2 = cleanEnd / eightSec;
    
    yesAlphaBand = ttest(cell2mat(alphaBand),cell2mat(beta1Band));
    if yesAlphaBand ==1 % Null hypothesis is rejected, Subject has  normal alpha bands
    
        for drowsyTest = 1:cleanEnd %8 sec at a time till the end of epochs
    %               convert the bands to matrix so we can perform operations on
    %               them.
            alphaBandCurrent{drowsyTest} = alphaBand{drowsyTest};
            thetaBandCurrent{drowsyTest} = thetaBand{drowsyTest};

            if drowsyTest <= (cleanEnd-(eightSec-1)) %if we haven't reached the last 8 sec

                %Comparing the current median of theta band ratio over median of alpha band current to the ratio of
    %                         start theta band pow over start of alpha band pow

                if (thetaBandCurrent{drowsyTest}/alphaBandCurrent{drowsyTest}) > meanOfStartThetaPow/meanOfStartAlphaPow
                    counter = counter +1;
                    if counter < eightSec
                        cleanArray{2,drowsyTest} = 'Light D';
                        lightDrowsy = [lightDrowsy drowsyTest];
                    else 
                        cleanArray{2,drowsyTest} = '8sec Deep D';
                        deepDrowsy = [deepDrowsy drowsyTest];
                        stateChange = stateChange+1;
                        counter = 0;
                    end
                else if (thetaBandCurrent{drowsyTest}/alphaBandCurrent{drowsyTest}) < meanOfStartThetaPow/meanOfStartAlphaPow
                    if counter == 1
                        cleanArray{2,drowsyTest} = 'Arousal';
                        arousal = [arousal drowsyTest];
                        counter = 0;
                    else
                        cleanArray{2,drowsyTest} = 'Awake';
                        awake = [awake drowsyTest];
                    end
                    end

                end


            else
                break;% once no more 8 sec segments left exit the loop
            end   

        end
            
        if stateChange/cleanEnd2 >= 0 && stateChange/cleanEnd2 <= .2499999999
            freqDrowsinessScore = 1; 
            elseif stateChange/cleanEnd2 >= .25 && stateChange/cleanEnd2 <= .4999999999
                freqDrowsinessScore = 2; 
            elseif stateChange/cleanEnd2 >= .50 && stateChange/cleanEnd2 <= .7499999999
                freqDrowsinessScore = 3; 
            elseif stateChange/cleanEnd2 >= .75 && stateChange/cleanEnd2 <= 1
                freqDrowsinessScore = 4;
        else     
            freqDrowsinessScore = 0; 
        end
        
    else % Subject doesn't have alpha band
%       
        disp('Subject does not have good alpha bands. ');%Eye blink score will be dominant.
        stateChange = 0;
        for drowsyTest = 1:cleanEnd

            beta1BandCurrent{drowsyTest} = beta1Band{drowsyTest};
            thetaBandCurrent{drowsyTest} = thetaBand{drowsyTest};

            if drowsyTest <= (cleanEnd-(eightSec-1)) %if we haven't reached the last 8 sec

                %Comparing the current median of theta band ratio over median of alpha band current to the ratio of
    %                         start theta band pow over start of alpha band pow

                if (thetaBandCurrent(drowsyTest)/beta1BandCurrent(drowsyTest)) > meanOfStartThetaPow/meanOfStartBeta1Pow %ratio of theta/betha1  
                    counter = counter +1;
                    if counter < eightSec
                        cleanArray{2,drowsyTest} = 'Light D';
                        lightDrowsy = [lightDrowsy drowsyTest];
                    else 
                        cleanArray{2,drowsyTest} = '8sec Deep D';
                        deepDrowsy = [deepDrowsy drowsyTest];
                        stateChange = stateChange+1;
                        counter = 0;
                    end
                else if (thetaBandCurrent{drowsyTest}/beta1BandCurrent{drowsyTest}) < meanOfStartThetaPow/meanOfStartBeta1Pow
                        if counter == 1
                            cleanArray{2,drowsyTest} = 'Arousal';
                            arousal = [arousal drowsyTest];
                            counter = 0;
                        else
                            cleanArray{2,drowsyTest} = 'Awake';
                            awake = [awake drowsyTest];
                        end
                    end

                end

            else
                break;% once no more 8 sec segments left exit the loop
            end 

        end % end of State detenction
        % This case weights the eye blinks the most at 70%

       
        if stateChange/cleanEnd2 >= 0 && stateChange/cleanEnd2 <= .2499999999
            freqDrowsinessScore = 1; 
            elseif stateChange/cleanEnd2 >= .25 && stateChange/cleanEnd2 <= .4999999999
                freqDrowsinessScore = 2; 
            elseif stateChange/cleanEnd2 >= .50 && stateChange/cleanEnd2 <= .7499999999
                freqDrowsinessScore = 3; 
            elseif stateChange/cleanEnd2 >= .75 && stateChange/cleanEnd2 <= 1
                freqDrowsinessScore = 4;
        else     
            freqDrowsinessScore = 0; 
        end

    end % end of script for individuals with no alpha band and medication
      

    
    catch exception
    
        disp(exception);

        keyboard;

end

