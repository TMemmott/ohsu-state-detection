function [ FzAntAlphaBand, MeanOfStartOfFzAntAlphaBand, AntCleanArray, antAlpha8sec, antAlphaFz, NoAntAlpha, AnteriorizationOfAlphaScore ] = AntOfAlphaScore( anteriorAlphaBand, cleanEnd, eightSec, thirtyTwoSec, cleanEnd2, AntCleanArray  )
try
    %% 
    anteriorizationOfAlphaCounter = 0;
    matAntAlphaBand = cell2mat(anteriorAlphaBand);
    
    % get the alpha band for all midline channels
    FzAntAlphaBand = matAntAlphaBand(1:4:max(size(matAntAlphaBand))-3);
    CzAntAlphaBand = matAntAlphaBand(2:4:max(size(matAntAlphaBand))-2);
    PzAntAlphaBand = matAntAlphaBand(3:4:max(size(matAntAlphaBand)-1));
    OzAntAlphaBand = matAntAlphaBand(4:4:max(size(matAntAlphaBand)));
    
    % calculate the mean alpha in midline channles     
    MeanOfStartOfFzAntAlphaBand = mean(FzAntAlphaBand(1:thirtyTwoSec));
    MeanOfStartOfCzAntAlphaBand = mean(CzAntAlphaBand(1:thirtyTwoSec));
    MeanOfStartOfPzAntAlphaBand = mean(PzAntAlphaBand(1:thirtyTwoSec));
    MeanOfStartOfOzAntAlphaBand = mean(OzAntAlphaBand(1:thirtyTwoSec));
    
    % Initialize some arrays etc. for scoring
    counter2 = 0;
    antAlpha8sec = [];
    antAlphaFz = [];
    NoAntAlpha = [];
 
    for AntTest = 1:cleanEnd %32 sec at a time till the end of epochs
%               convert the bands to matrix so we can perform operations on
%               them.
        if AntTest <= (cleanEnd-(eightSec-1)) %if we haven't reached the last 32 sec

          if  (OzAntAlphaBand(AntTest) < MeanOfStartOfOzAntAlphaBand || PzAntAlphaBand(AntTest) < MeanOfStartOfPzAntAlphaBand) && (CzAntAlphaBand(AntTest) > MeanOfStartOfCzAntAlphaBand || FzAntAlphaBand(AntTest) > MeanOfStartOfFzAntAlphaBand)
%               AntCleanArray{2,AntTest} = 'Inc. in Oz';
%             if PzAntAlphaBand(AntTest) > MeanOfStartOfPzAntAlphaBand
%                 AntCleanArray{2,AntTest} = 'Inc. in Pz';
%                 if CzAntAlphaBand(AntTest) > MeanOfStartOfCzAntAlphaBand
%                     AntCleanArray{2,AntTest} = 'Inc. in Cz';
%                     if FzAntAlphaBand(AntTest) > MeanOfStartOfFzAntAlphaBand
                        AntCleanArray{2,AntTest} = 'Anteriorization';  
                        counter2 = counter2 +1;
                        antAlphaFz = [antAlphaFz AntTest];
                        if counter2 >= eightSec
                            AntCleanArray{2,AntTest} = '8sec Ant';
                            anteriorizationOfAlphaCounter = anteriorizationOfAlphaCounter+1;
                            antAlpha8sec = [antAlpha8sec AntTest];
                            counter2 = 0;
                        end
%                     else
%                         AntCleanArray{2,AntTest} = 'No Ant.ofAlpha';
%                     end
%                 end
%              end
          else
              AntCleanArray{2,AntTest} = 'No Anteriorization';
              NoAntAlpha = [NoAntAlpha AntTest];
              counter2 = 0;
          end
   
       else
            break;% once no more 8 sec segments left exit the loop
       end   
    end


    
    if anteriorizationOfAlphaCounter/cleanEnd2 >= 0 && anteriorizationOfAlphaCounter/cleanEnd2 <= .2499999999
        AnteriorizationOfAlphaScore = 1; 
        elseif anteriorizationOfAlphaCounter/cleanEnd2 >= .25 && anteriorizationOfAlphaCounter/cleanEnd2 <= .4999999999
            AnteriorizationOfAlphaScore = 2;
        elseif anteriorizationOfAlphaCounter/cleanEnd2 >= .50 && anteriorizationOfAlphaCounter/cleanEnd2 <= .7499999999
            AnteriorizationOfAlphaScore = 3; 
        elseif anteriorizationOfAlphaCounter/cleanEnd2 >= .75 && anteriorizationOfAlphaCounter/cleanEnd2 <= 1
            AnteriorizationOfAlphaScore = 4;
    else     
        AnteriorizationOfAlphaScore = 0; 
    end 

    catch exception
    
        disp(exception);
        keyboard;

end

