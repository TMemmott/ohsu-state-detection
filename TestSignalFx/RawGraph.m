function [ range ] = RawGraph( TransMatCleanRawEpochs, TransMatCleanBlinkEpochs )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
try
    %% 
    epoch = input('Which epoch would you like to see?');
    range = ((epoch-1)*4000+1):(epoch*4000);
    figure (1)% Channels f3, f4, c3, c4, p3, p4, o1, and o2
    % Need to add a section to calculate the time window for the epoch user
    % wants to see. There are 8 columns in the TransMatCleanRawEpochs matrix,
    % and the order of the channels are: f3, f4, c3, c4, p3, p4, o1, and o2
    subplot(1,2,1)
    strips(TransMatCleanRawEpochs(range,[1,2]),2,300,1.5), grid ON, legend('f3','f4'), ...
           xlabel('Time (sec)'), ylabel('each line one epoch');
    subplot(1,2,2)
    strips(TransMatCleanRawEpochs(range,[3,4]),2,300,1.5), grid ON, legend('c3','c4'), ...
            xlabel('time (sec)'), ylabel('each line one epoch');

    figure (2)% Channels f3, f4, c3, c4, p3, p4, o1, and o2
    subplot(1,2,1)
    strips(TransMatCleanRawEpochs(range,[5,6]),2,300,1.5), grid ON, legend('p3','p4'), ...
           xlabel('Time (sec)'), ylabel('each line one epoch');
    subplot(1,2,2)
    strips(TransMatCleanRawEpochs(range,[7,8]),2,300,1.5), grid ON, legend('o1','o2'), ...
            xlabel('time (sec)'), ylabel('each line one epoch');

    figure(3)% Raw blinks TransMatCleanBlinkEpochs
    strips(TransMatCleanBlinkEpochs(range,:),4,300), grid ON, legend('Fp1', 'Fp2'), ...
           xlabel('Time (sec)'), ylabel('each line one epoch');
    
    
    
    catch exception
  
        disp(exception);
        keyboard;
end

