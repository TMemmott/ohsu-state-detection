function [windowedEpochs, dataFFT, meanDataFFT, power, anteriorPower, iPower, medianOzFrequency] = calculateFrequencyMeasures(nuEpochs, Windowing, preHanningEPOCHS, segLength, fs, channelNumber)

try
    windowedEpochs = cell(1,nuEpochs);
    dataFFT = cell(1, nuEpochs);
    meanDataFFT = cell(1, nuEpochs);
    power = cell(1, nuEpochs);
    anteriorPower = cell(1, nuEpochs);
    medianOzFrequency = cell(1, nuEpochs);
   
    
    
    for iPower = 1:nuEpochs
        if isempty (preHanningEPOCHS{iPower}) == 0
           % 
            windowedEpochs{iPower} = preHanningEPOCHS{iPower}.*Windowing;
            % FFT of each epoch at every msec
            dataFFT{iPower} = fft(windowedEpochs{iPower}, segLength);
            % Average of FFT of Fz, Cz, Pz and Oz
            meanDataFFT{iPower} = mean(dataFFT{iPower},2);
            power{iPower} = (abs(meanDataFFT{iPower})).^2;
            % Also calculating power in each channel to the anteriorization
            % check algorithm
            anteriorPower{iPower}=(abs(dataFFT{iPower})).^2;
            % Median freq of each epoch
            medianOzFrequency{iPower} = medfreq(windowedEpochs{iPower}(:,channelNumber),fs);

            % for calculating power, we can also use the function
            %         power2{i}=periodogram(mean(windowedEpochs{i},2)); 
            % periodogram takes the input signal and returns the power spectral
            % density. 
        else
            break
        end
    end
catch exception
    
    disp(exception);
    
    keyboard;
end