function [ cleanMedianOzFrequency, medianFreqDrowsinessScore, MeanOfStartOfMedianOzFrequencies, DecreaseOzMedPowFreq, EightsecDecreaseOzMedPowFreq, NoDecOzMedPowFreq, DecreaseMedianFreqScore] = MedFreqOfPow( cleanMedianOzFrequency, cleanEnd, eightSec, thirtyTwoSec, cleanEnd2 )
try
%% 
 % Median Frequency Script using Oz channel. 
    matCleanMedianOzFrequency = cell2mat(cleanMedianOzFrequency');
    step = 1;
    
    if cleanMedianOzFrequency{1,1} < 5
        step = 2;
        thirtyTwoSec = thirtyTwoSec + 1;
    elseif cleanMedianOzFrequency{1, 2} < 5
        step = 3;
        thirtyTwoSec = thirtyTwoSec + 2;
    elseif cleanMedianOzFrequency{1, 3} < 5
        step = 4;
        thirtyTwoSec = thirtyTwoSec + 3;
    elseif cleanMedianOzFrequency{1, 4} < 5
        step = 5;
        thirtyTwoSec = thirtyTwoSec + 4;
    elseif cleanMedianOzFrequency{1, 5} < 5
        step = 6;
        thirtyTwoSec = thirtyTwoSec + 5;
    end
    
    MeanOfStartOfMedianOzFrequencies = mean(matCleanMedianOzFrequency(step:thirtyTwoSec),1);
    DecreaseMedianFreq = 0;
    EightsecDecreaseOzMedPowFreq = [];
    DecreaseOzMedPowFreq = [];    
    NoDecOzMedPowFreq=[];
    
    counter =0;
    for medianPowerInterval  = 1:cleanEnd %8 sec at a time till the end of epochs
%               convert the bands to matrix so we can perform operations on
%               them.
        if medianPowerInterval  <= (cleanEnd-(eightSec)) %if we haven't reached the last 8 sec
            if matCleanMedianOzFrequency(medianPowerInterval ) < MeanOfStartOfMedianOzFrequencies
                cleanMedianOzFrequency{2,medianPowerInterval } = 'Decrease';
                counter = counter+1;
                DecreaseOzMedPowFreq = [DecreaseOzMedPowFreq medianPowerInterval];
                
                if counter >=eightSec
                    cleanMedianOzFrequency{2,medianPowerInterval } = '8sec Decrease';
                    DecreaseMedianFreq = DecreaseMedianFreq+1;
                    EightsecDecreaseOzMedPowFreq = [EightsecDecreaseOzMedPowFreq medianPowerInterval];
                    counter = 0;
                end
            else
                cleanMedianOzFrequency{2,medianPowerInterval } = 'No Dec.';
                NoDecOzMedPowFreq = [NoDecOzMedPowFreq medianPowerInterval ];
                counter = 0;

            end
        else
            break;% once no more 8 sec segments left exit the loop
        end  
    end
    
%     significantChangeinState3 = [];
%     for medianPowerInterval  = 1:cleanEnd
%         if MeanOfStartOfMedianOzFrequencies < cleanMedianOzFrequency{1, medianPowerInterval}
%             significantChangeinState3 = [significantChangeinState3 medianPowerInterval];
%         end 
%     end
    % See how many epochs resulted in a change in state flag          
    [~, stateChange3] = size(DecreaseOzMedPowFreq);

    % calculate the percentage change         
    percentChange = stateChange3/cleanEnd;
    
    

    if DecreaseMedianFreq/cleanEnd2 >= 0 && DecreaseMedianFreq/cleanEnd2 <= .2499999999
        DecreaseMedianFreqScore = 1; 
    elseif DecreaseMedianFreq/cleanEnd2 >= .25 && DecreaseMedianFreq/cleanEnd2 <= .4999999999
        DecreaseMedianFreqScore = 2;
    elseif DecreaseMedianFreq/cleanEnd2 >= .50 && DecreaseMedianFreq/cleanEnd2 <= .7499999999
        DecreaseMedianFreqScore = 3; 
    elseif DecreaseMedianFreq/cleanEnd2 >= .75 && DecreaseMedianFreq/cleanEnd2 <= 1
        DecreaseMedianFreqScore = 4;
    else     
        DecreaseMedianFreqScore = 0; 
    end 

    % loop through the median frequencies to score
    if percentChange >= .25 && percentChange <= .299999
        medianFreqDrowsinessScore = 1; 
    elseif percentChange >= .30 && percentChange <= .69999999
        medianFreqDrowsinessScore = 2; 
    elseif percentChange >= .70 && percentChange <= .79999999
        medianFreqDrowsinessScore = 3;
     elseif percentChange >= .80
        medianFreqDrowsinessScore = 4;
    else     
        medianFreqDrowsinessScore = 0; 
    end 
    
    if length(EightsecDecreaseOzMedPowFreq) > 5 && medianFreqDrowsinessScore < 4
        medianFreqDrowsinessScore = medianFreqDrowsinessScore + 1;
    end
    
    catch exception
  
        disp(exception);
        keyboard;

end

