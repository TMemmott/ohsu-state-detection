function [p3, c3, f3, fz, f4, c4, p4, cz, a1, Fp1, Fp2, t3, t5, o1, o2, f7, f8, a2, t6, t4] = getChannels(EEG)
% getChannels takes in EEG and defines channels for later processing

    p3 = EEG(:,1);
    c3 = EEG(:,2);
    f3 = EEG(:,3);
    fz = EEG(:,4);
    f4 = EEG(:,5);
    c4 = EEG(:,6);
    p4 = EEG(:,7);
    cz = EEG(:,8);
    a1 = EEG(:,9);
    Fp1 = EEG(:,10);
    Fp2 = EEG(:,11);
    t3 = EEG(:,12);
    t5 = EEG(:,13);
    o1 = EEG(:,14);
    o2 = EEG(:,15);
    f7 = EEG(:,16);
    f8 = EEG(:,17);
    a2 = EEG(:,18);
    t6 = EEG(:,19);
    t4 = EEG(:,20);

end

